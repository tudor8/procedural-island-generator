# Procedural Island Generator

Repository for my Procedural Island Generator.

An example can be found at http://www.tudorgheorghe.net

# Getting Started

* This is procedural generator that can generate islands based on many parameters.

* Different islands can be achieved by changing the settings of the Generator and Noise Generator scripts.

* This also uses a procedural Tree Generator to populate the island with trees.

# Running

Currently this can only be ran inside of the Unity Editor.

# Built With

* [Unity](https://unity.com/) - Main Engine

# Authors

* **Tudor Gheorghe** - (http://www.tudorgheorghe.net)
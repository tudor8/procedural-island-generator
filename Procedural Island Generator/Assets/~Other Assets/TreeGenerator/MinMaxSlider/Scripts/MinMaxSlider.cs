﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class MinMaxSlider : MonoBehaviour, IPointerDownHandler, IDragHandler {
	public enum Type {
		Left, Right
	}
	[SerializeField] MinMaxSliderLeftHandle  minHandle;
	[SerializeField] MinMaxSliderRightHandle maxHandle;
	[SerializeField] MinMaxSliderFill fill;
	[SerializeField] RectTransform bounds;

	[SerializeField] float minValue = 0.00f;
	[SerializeField] float maxValue = 1.00f;
	[SerializeField] float currentMin = 0.3f;
	[SerializeField] float currentMax = 0.6f;
	[SerializeField] float minDifference = 0.30f;

	[SerializeField] UnityEvent onValueChanged;

	[SerializeField][HideInInspector] Type selectedType;

	public MinMaxSliderLeftHandle MinHandle { 
		get { return minHandle; }
	}

	public UnityEvent OnValueChanged { 
		get { return onValueChanged; }
	}

	public MinMaxSliderRightHandle MaxHandle { 
		get { return maxHandle; }
	}

	public float MinDifference { 
		get { return minDifference; }
	}

	public float MinValue { 
		get { return minValue; }
		set { minValue = value; }
	}

	public float CurrentMin { 
		get { return currentMin; }
		set { currentMin = value; }
	}

	public float MaxValue { 
		get { return maxValue; }
		set { maxValue = value; }
	}

	public float CurrentMax { 
		get { return currentMax; }
		set { currentMax = value; }
	}

	void Awake () {
		minHandle.RectTransform.localPosition = new Vector3(fill.RectTransform.sizeDelta.x * currentMin, 0);
		maxHandle.RectTransform.localPosition = new Vector3(fill.RectTransform.sizeDelta.x * currentMax, 0);
		UpdateFill ();
	}

	public void OnPointerDown(PointerEventData eventData) {
		Vector2 position;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle (bounds, eventData.position, eventData.pressEventCamera, out position)) {
			if (position.x < fill.RectTransform.localPosition.x) {
				minHandle.OnDrag (eventData);
				selectedType = Type.Left;
			}
			if (position.x > fill.RectTransform.localPosition.x) {
				maxHandle.OnDrag (eventData);
				selectedType = Type.Right;
			}

			onValueChanged.Invoke ();
		}
	}

	public void OnDrag(PointerEventData eventData) {
		Vector2 position;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle (bounds, eventData.position, eventData.pressEventCamera, out position)) {
			switch (selectedType) {
			case Type.Left:
				minHandle.OnDrag (eventData);
				break;
			case Type.Right:
				maxHandle.OnDrag (eventData);
				break;
			}

			onValueChanged.Invoke ();
		}
	}

	public void UpdateFill() {
		fill.RectTransform.sizeDelta = new Vector2(maxHandle.RectTransform.localPosition.x - minHandle.RectTransform.localPosition.x, fill.RectTransform.sizeDelta.y);
		fill.RectTransform.localPosition = minHandle.RectTransform.localPosition + new Vector3(fill.RectTransform.sizeDelta.x, 0) * 0.5f;
	}

	public void UpdateHandlers() {
		Vector3 halfWidth = new Vector3(fill.RectTransform.sizeDelta.x, 0) * 0.5f;
		minHandle.RectTransform.localPosition = fill.RectTransform.localPosition - halfWidth;
		maxHandle.RectTransform.localPosition = fill.RectTransform.localPosition + halfWidth;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MinMaxSliderFill : Selectable, IDragHandler {
	[SerializeField] MinMaxSlider minMaxSlider;
	[SerializeField] RectTransform bounds;
	[SerializeField] RectTransform rectTransform;

	public RectTransform RectTransform {
		get { return rectTransform; }
	}

	public void OnDrag(PointerEventData eventData) {
		Vector2 position;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle(bounds, eventData.position, eventData.pressEventCamera, out position)) {
			float halfWidth = rectTransform.rect.width / 2;

			if(position.x < bounds.rect.xMin + halfWidth) position.x = bounds.rect.xMin + halfWidth;
			if(position.x > bounds.rect.xMax - halfWidth) position.x = bounds.rect.xMax - halfWidth;

			position.y = 0;

			minMaxSlider.CurrentMin = Mathf.Clamp01((position.x - halfWidth) / bounds.rect.width);
			minMaxSlider.CurrentMax = Mathf.Clamp01((position.x + halfWidth) / bounds.rect.width);

			rectTransform.localPosition = position;

			minMaxSlider.UpdateHandlers ();

			minMaxSlider.OnValueChanged.Invoke ();
		}
	}
}

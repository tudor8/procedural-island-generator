﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TreeGenerator {
	/**
	 * Simple camera script to lerp between 2 places and 2 focus points
	 * Use Percentage to move the camera
	 */
	public class CameraScript : MonoBehaviour {
		[SerializeField] GameObject firstFocus ;
		[SerializeField] GameObject secondFocus;
		[SerializeField] GameObject firstPos ;
		[SerializeField] GameObject secondPos;
		[SerializeField] float percentage;

		public float Percentage { 
			get { return percentage; }
			set { percentage = Mathf.Clamp01 (value); }
		}

		void Update () {
			transform.position = Vector3.Lerp (firstPos.transform.position, secondPos.transform.position, percentage);

			transform.LookAt (Vector3.Lerp(firstFocus.transform.position, secondFocus.transform.position, percentage));
		}
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TreeGenerator {
	[System.Serializable]
	public class BranchSegment {
		TreeScript treeScript;
		//Branch branch;
		[SerializeField] Vector3 center   ; // The center of this segment
		[SerializeField] Vector3 direction; // The direction the segment is pointing towards
		[SerializeField] Vector3 directionFront; // The direction the segment is pointing towards
		[SerializeField] int[]   points   ; // References to the vertices
		[SerializeField] int[]   colors   ; // References to the colors
		[SerializeField] Vector3[] pointDirections;
		[SerializeField] float   width    ; // Distance from the center to the points
		[SerializeField] float   height   ; // Distance from the previous segment
		[SerializeField] Vector2 scale    ; // Width/Height scale based on the tree width/height
		[SerializeField] bool changed;
		[SerializeField] MeshData meshData;

		public Vector3 Center    { get { return center   ; } set { center    = value; } }
		public Vector3 Direction { get { return direction; } set { direction = value; } }
		public Vector3 DirectionFront { get { return directionFront; } set { directionFront = value; } }
		public int[]   Points    { get { return points   ; } set { points    = value; } }
		public int[]   Colors    { get { return colors   ; } set { colors    = value; } }
		public float   Width     { get { return width    ; } set { width     = value; } }
		public float   Height    { get { return height   ; } set { height    = value; } }
		public Vector2 Scale     { get { return scale    ; } set { scale     = value; } }
		public bool    Changed   { get { return changed  ; } set { changed   = value; } }
		public MeshData    MeshData   { get { return meshData  ; } set { meshData   = value; } }

		public BranchSegment (TreeScript treeScript, MeshData meshData, Vector3 center, Vector3 directionTop, Vector3 directionFront, Vector2 scale, float uvPercentage) {
			this.changed = false;
			this.treeScript = treeScript;
			this.center = center;
			this.direction = directionTop;
			this.directionFront = directionFront;
			this.scale = scale;
			points = new int[treeScript.Sides * 2];
			pointDirections = new Vector3[treeScript.Sides * 2];
			this.meshData = meshData;

			float angles = 360 / treeScript.Sides;

			Vector3 position = directionFront * (treeScript.Width / 2 * scale.x);

			for (int i = 0; i < treeScript.Sides; i++) {
				for(int j = 0; j < 2; j++) {
					meshData.verticeList.Add (center + position);
					meshData.uvsList    .Add (new Vector2(uvPercentage, 0));
					points          [i * 2 + j] = meshData.verticeList.Count - 1;
					pointDirections [i * 2 + j] = position.normalized;
				}
				position = UsefulMethods.RotateAroundAxis (position, directionTop, angles);
			}
		}

		public BranchSegment(TreeScript treeScript) {
			this.treeScript = treeScript;
		}

		public void ResetHeight(BranchSegment previousSegment) {
			Vector3 newCenter = previousSegment.Center + direction * treeScript.Height * previousSegment.scale.y;
			float currentDistance = Vector3.Distance (center   , previousSegment.Center);
			float normalDistance  = Vector3.Distance (newCenter, previousSegment.Center);

			AddHeight (normalDistance - currentDistance);
		}

		public void AddHeight(float amount) {
			height += amount;
			center += direction * amount;
			for (int i = 0; i < points.Length; i++) {
				meshData.verticeList [points[i]] += direction * amount;
			}
		} 

		public void SetWidth(float amount) {
			width = amount;
			for (int i = 0; i < points.Length; i++) {
				meshData.verticeList [points [i]] = center + pointDirections[i] * amount;
			}
		}

		public void AddWidth(float amount) {
			width += amount;
			for (int i = 0; i < points.Length; i++) {
				meshData.verticeList [points [i]] += pointDirections[i] * amount;
			}
		}

		public static List<Triangle> TriangulateSegments(BranchSegment bottom, BranchSegment top) {
			List<Triangle> newTriangles = new List<Triangle> ();
			if (top.Points.Length != bottom.Points.Length) {
				Debug.LogError ("Top and bottom segments don't have the same amount of points");
			}
			for (int i = 0; i < top.treeScript.Sides; i++) {
				int index1 = (i * 2 + 1) % top.Points.Length;
				int index2 = (i * 2 + 2) % top.Points.Length;

				int x1, x2, x3;
				x1 = bottom.Points [index1];
				x2 = bottom.Points [index2];
				x3 = top   .Points [index1];

				newTriangles.Add(new Triangle(x1, x2, x3));

				x1 = top   .Points[index1];
				x2 = bottom.Points[index2];
				x3 = top   .Points[index2];

				newTriangles.Add(new Triangle(x1, x2, x3));
			}

			return newTriangles;
		}

		public static void TriangulateSegment(BranchSegment segment, float height) {
			Vector3 newCenter = segment.center + segment.direction * height;
			segment.meshData.verticeList.Add (newCenter);
			segment.meshData.uvsList.Add (Vector2.one);
			for (int i = 0; i < segment.treeScript.Sides; i++) {
				int index1 = (i * 2 + 1) % segment.Points.Length;
				int index2 = (i * 2 + 2) % segment.Points.Length;

				segment.meshData.triangleList.Add(new Triangle(segment.Points [index1], segment.Points [index2], segment.meshData.verticeList.Count - 1));
			}
		}
	}
}
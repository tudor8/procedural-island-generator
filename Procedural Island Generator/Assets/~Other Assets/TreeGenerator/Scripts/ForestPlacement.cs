﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TreeGenerator {
	public class ForestPlacement : MonoBehaviour {
		[SerializeField] GameObject treePrefab;

		public void PlaceDownTree(Vector3 position) {
			GameObject tree = Instantiate (treePrefab, position, Quaternion.identity) as GameObject;
			tree.GetComponent<TreeScript> ().Randomize ();
		}
		 
		public void PlaceDownTrees(List<Vector3> positions) { 
			for (int i = 0; i < positions.Count; i++) {
				GameObject tree = Instantiate (treePrefab, positions [i], Quaternion.identity) as GameObject;
				tree.GetComponent<TreeScript> ().Randomize ();
			}
		}
	}
}
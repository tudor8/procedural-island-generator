﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TreeGenerator {
	[System.Serializable]
	public class MinMaxFloat {
		[SerializeField] AnimationCurve randomnessCurve;
		[SerializeField] float min;
		[SerializeField] float max;

		public float Min { get { return min; } set { min = value; }}
		public float Max { get { return max; } set { max = value; } }

		public float GetRandom() {
			float random = Random.Range (min, max + 1);

			return random;
		}

		public float GetRandom(System.Random random) {
			float number = random.Next () * (max - min) + min;

			return number;
		}

		public float GetRandomWithCurve() {
			float random = Random.Range (0f, 1f);
			float result = randomnessCurve.Evaluate (random) * (max - min) + min;

			return result;
		}

		public float GetRandomWithCurve(System.Random random) {
			float number = (float) random.NextDouble ();
			float result = randomnessCurve.Evaluate (number) * (max - min) + min;

			return result;
		}
	}
}
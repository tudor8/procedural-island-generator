﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TreeGenerator {
	public class TextureGenerator : MonoBehaviour {
		[SerializeField] TextureWrapMode wrapMode   = TextureWrapMode.Clamp;
		[SerializeField] FilterMode      filterMode = FilterMode     .Point;

		public Texture2D GenerateTexture (Color[] pixels) {
			Texture2D texture = new Texture2D(100, 1);
			texture.name = "colorPallete";
			texture.SetPixels (pixels);
			texture.filterMode = filterMode;
			texture.wrapMode   = wrapMode  ;
			texture.Apply();

			return texture;
		}

		public Color[] GenerateColorPallete(Gradient gradient) {
			Color[] colors  = new Color[100];

			for (int i = 0; i < 100; i++) {
				colors [i] = gradient.Evaluate ((float) i / 100);
			}

			return colors;
		}
	}
}
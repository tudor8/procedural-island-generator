﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace TreeGenerator {
	[CustomEditor(typeof(TreeScript))]
	public class TreeScriptEditor : Editor {
		public override void OnInspectorGUI () {
			TreeScript script = (TreeScript)target;
			DrawDefaultInspector ();
			if (GUILayout.Button ("Generate")) {
				script.Generate ();
			}
			if (GUILayout.Button ("Randomize")) {
				script.Randomize ();
			}
			if (GUILayout.Button ("Set To Stage")) {
				script.SetToPercentage (script.stageToSetTo);
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

/**
 * UI class to represent the Color Controller class.
 * Contains methods should be called on OnValueChange events from sliders.
 * A locking field stops UpdateSliders() from calling the events on those sliders over and over.
 * Generates the main display texture and the hue slider texture on Start.
 * 
 * Author - Tudor Gheorghe
 */
public class ColorControllerUI : MonoBehaviour {
	[SerializeField] ColorController colorController;
	[SerializeField] ColorPicker colorPicker;
	[SerializeField] Slider sideSlider;
	[SerializeField] Slider hueSlider;
	[SerializeField] Slider saturationSlider;
	[SerializeField] Slider valueSlider;
	[SerializeField] Slider redSlider;
	[SerializeField] Slider greenSlider;
	[SerializeField] Slider blueSlider;
	[SerializeField] Slider alphaSlider;
	[SerializeField] Image mainImage;
	[SerializeField] Image rgbImage;
	[SerializeField] Image currentColorNormal;
	[SerializeField] Image currentColorAlpha ;

	[SerializeField] UnityEvent onChangeEvents; // Additional events to call whenever something changes

	[SerializeField][HideInInspector] bool locked; // Will be true during an operation and false after

	public ColorController ColorController {
		get { return colorController; }
	}

	void Start() {
		colorController.GenerateTexture ();

		GenerateHueScrollerTexture ();

		mainImage.sprite = Sprite.Create(colorController.MainTexture, new Rect(0f, 0f, colorController.Size, colorController.Size), Vector2.one * 0.5f, colorController.Size);

		UpdateReferences ();
	}

	/**
	 * The side slider contains all the colors in a rainbow. Moving it does the same function as moving the hueSlider;
	 */
	public void GenerateHueScrollerTexture () {
		Color[] colors = new Color[colorController.Size];
		bool isHorizontal = (sideSlider.direction == Slider.Direction.LeftToRight || sideSlider.direction == Slider.Direction.RightToLeft);
		Texture2D sideTexture;
		if (isHorizontal) {sideTexture = new Texture2D (colorController.Size, 1);} 
		else              {sideTexture = new Texture2D (1, colorController.Size);}

		for (var i = 0; i < colorController.Size; i++) {
			colors[i] = Color.HSVToRGB((float) i / (colorController.Size - 1), 1f, 1f);
		}

		sideTexture.SetPixels(colors);
		sideTexture.Apply();

		rgbImage.sprite = Sprite.Create(sideTexture, new Rect(0f, 0f, sideTexture.width, sideTexture.height), Vector2.one * 0.5f, colorController.Size);
	}

	/*********************************************************************************************************************************************************
	 * Setters to be used by the sliders
	 *********************************************************************************************************************************************************/
	public void SetHue(float value) {
		if (locked) return;
		else locked = true;

		colorController.Hue = value;
		UpdateReferences ();
	}

	public void SetSaturation(float value) {
		if (locked) return;
		else locked = true;

		colorController.Saturation = value;
		UpdateReferences ();
	}

	public void SetValue(float value) {
		if (locked) return;
		else locked = true;

		colorController.Value = value;
		UpdateReferences ();
	}

	public void SetRed  (float value) {
		if (locked) return;
		else locked = true;

		colorController.Red   = value;
		UpdateReferences ();
	}

	public void SetGreen(float value) {
		if (locked) return;
		else locked = true;

		colorController.Green = value;
		UpdateReferences ();
	}

	public void SetBlue (float value) {
		if (locked) return;
		else locked = true;

		colorController.Blue  = value;
		UpdateReferences ();
	}

	public void SetAlpha(float value) {
		if (locked) return;
		else locked = true;

		colorController.Alpha  = value;
		UpdateReferences ();
	}

	public void SetColor(Color color) {
		if (locked) return;
		else locked = true;

		colorController.SetToColor (color);
		UpdateReferences ();
	}

	/**
	 * Update whatever references (example sliders) are properly set to the values in the controller
	 */
	protected void UpdateReferences() {
		if (sideSlider != null) sideSlider.value = colorController.Hue;
		if (hueSlider != null) hueSlider.value = colorController.Hue;
		if (saturationSlider != null) saturationSlider.value = colorController.Saturation;
		if (valueSlider != null) valueSlider.value = colorController.Value;
		if (redSlider != null) redSlider.value = colorController.Red;
		if (blueSlider != null) blueSlider.value = colorController.Blue;
		if (greenSlider != null) greenSlider.value = colorController.Green;
		if (alphaSlider != null) alphaSlider.value = colorController.Alpha;
		if (currentColorNormal != null) {
			Color colorToSetTo = colorController.SelectedColor;
			colorToSetTo.a = 1;
			currentColorNormal.color = colorToSetTo;
		}
		if (currentColorAlpha != null) currentColorAlpha.fillAmount = colorController.Alpha;

		if (colorPicker != null) {
			// Reset the alpha so the display shows properly
			Color colorToSetTo = colorController.SelectedColor;
			colorToSetTo.a = 1;
			colorPicker.CurrentColor.color = colorToSetTo;
			colorPicker.transform.localPosition = new Vector3 (
				colorController.Saturation * colorPicker.Transform2D.rect.width, 
				colorController.Value * colorPicker.Transform2D.rect.height
			);
		}
			
		onChangeEvents.Invoke ();

		locked = false;
	}
	/********************************************************************************************************************************************************/
}
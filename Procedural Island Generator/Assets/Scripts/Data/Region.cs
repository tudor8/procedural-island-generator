﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Region {
	public string name  ;
	public float  height;
	public Color  color ;

	public Region(string name, float height, Color color) {
		this.name = name;
		this.height = height;
		this.color = color;
	}

	public static Region Void { get { return new Region ("Void", -1f, Color.black); } }

	public static Region GetCorrespondingRegion(float value, Region[] regions) {
		for (int i = 0; i < regions.Length; i++) {
			if (value <= regions [i].height) {
				return regions[i];
			}
		}

		return regions.Length == 0 ? null : regions [regions.Length - 1];
	}

	public static int GetCorrespondingIndex(float value, Region[] regions) {
		for (int i = 0; i < regions.Length; i++) {
			if (value <= regions [i].height) {
				return i;
			}
		}
		Debug.LogError ("No corresponding region");

		return regions.Length == 0 ? -1 : regions.Length - 1;
	}

	public static string[] GetRegionNames(Region[] regions) {
		string[] names = new string[regions.Length];
		for (int i = 0; i < regions.Length; i++) {
			names [i] = regions [i].name;
		}
		return names;
	}
}
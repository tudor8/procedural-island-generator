﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DisplayableMapData {
	[SerializeField] int seed = 0;
	[SerializeField] AnimationCurve curve;
	[SerializeField] float heightModifier;
	[SerializeField] Region[] regions;
	[SerializeField] Vector2 offset;

	[SerializeField][HideInInspector] MapData mapData;

	public int Seed {
		get { return seed; }
		set { seed = value; }
	}

	public AnimationCurve Curve {
		get { return curve; }
	}

	public float HeightModifier {
		get { return heightModifier; }
	}

	public Region[] Regions {
		get { return regions; }
	}

	public Vector2 Offset {
		get { return offset; }
	}

	public MapData MapData {
		get { return mapData; }
		set { mapData = value; }
	}
}

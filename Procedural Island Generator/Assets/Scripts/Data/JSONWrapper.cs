﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class JSONWrapper {
	public Vector3   position   ;
	public string    textureName;
	public Vector3[] vertices   ;
	public Vector2[] uvs        ;
	public int    [] triangles  ;

	public JSONWrapper(Vector3 position, string textureName, Vector3[] vertices, Vector2[] uvs, int[] triangles) {
		this.position    = position   ;
		this.textureName = textureName;
		this.vertices    = vertices   ;
		this.uvs         = uvs        ;
		this.triangles   = triangles  ;
	}

	public static JSONWrapper FromMesh(MeshFilter mesh, string textureName) {
		return new JSONWrapper (mesh.transform.position, textureName, mesh.sharedMesh.vertices, mesh.sharedMesh.uv, mesh.sharedMesh.triangles);
	}

	public string ToJSON() {
		return JsonUtility.ToJson (this);
	}
}
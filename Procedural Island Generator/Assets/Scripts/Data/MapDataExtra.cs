﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MapDataExtra {
	public class Tile {
		public float  value ;
		public Region region;
		public bool   inUse ;
		public List<Triangle> triangles; // Triangles that compose this tile

		public Tile(float value, Region region, bool inUse, List<Triangle> triangles) {
			this.value = value;
			this.region = region;
			this.inUse = inUse;
			this.triangles = triangles;
		}
	}

	public MapData mapData;
	public Dictionary<int, MeshData>[,] verticeReferences;

	public Tile     [,] tiles;
	public string   [,] tileTypes;

	public MapDataExtra(MapData mapData, Vector2Int tileSize) {
		Vector2Int actualSize = tileSize + Vector2Int.one;

		this.mapData = mapData;
		verticeReferences = new Dictionary<int, MeshData>[actualSize.x, actualSize.y];

		tiles     = new Tile  [tileSize.x, tileSize.y];
		tileTypes = new string[tileSize.x, tileSize.y];


		UsefulMethods.ForEachElement(actualSize, (position) => {
			verticeReferences [position.x, position.y] = new Dictionary<int, MeshData>();
		});
	}

	public void SetUpTile(int x, int y, float value, Region region, bool inUse, List<Triangle> triangles) {
		tiles [x, y] = new Tile (value, region, inUse, triangles);
	}

	public void SetUpChunk(Mesh mesh, Vector2Int offset, Vector2Int size, Region[] regions) {
		MeshData meshData = MeshData.FromMesh (mesh);
		List<Triangle> triangles = Triangle.GetTriangleList (meshData.triangles);
		int index = 0;
		for (int y = 0; y < size.y; y++) {
			for (int x = 0; x < size.x; x++) {
				int xCoord = x + offset.x;
				int yCoord = y + offset.y;
				float value = mapData.Get (xCoord, yCoord);
				Region region = Region.GetCorrespondingRegion (value, regions);
				SetUpTile(xCoord, yCoord, value, region, true, triangles.GetRange(index, 2));
				index += 2;
			}
		}
	}
}

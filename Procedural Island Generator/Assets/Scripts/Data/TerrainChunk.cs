﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainChunk : MonoBehaviour {
	[SerializeField] MeshFilter   meshFilter  ;
	[SerializeField] MeshRenderer meshRenderer;
	[SerializeField] MeshData     meshData;

	public MeshFilter MeshFilter {
		get { return meshFilter; }
	}

	public MeshRenderer MeshRenderer {
		get { return meshRenderer; }
	}

	public MeshData MeshData {
		get { return meshData; }
		set { meshData = value; }
	}

	public void Initialize(DisplayableMeshData meshData) {
		this.meshData                    = meshData.MeshData;
		this.meshFilter  .sharedMesh     = meshData.MeshData.ToMesh();
		this.meshRenderer.sharedMaterial = meshData.Material;
	}
}
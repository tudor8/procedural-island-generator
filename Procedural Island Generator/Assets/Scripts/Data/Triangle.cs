﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Holds references to 3 indexes of vertices. 
 * Basically a wrapper for triangle indexes to make it a bit more readable.
 */
public class Triangle {
	public int v1, v2, v3;
	public Vector2 index;
	public Triangle(int v1, int v2, int v3) {
		this.v1 = v1;
		this.v2 = v2;
		this.v3 = v3;
	}
	public Triangle(int v1, int v2, int v3, Vector2 index) {
		this.v1 = v1;
		this.v2 = v2;
		this.v3 = v3;
		this.index = index;
	}

	public void Subtract(int value) {
		v1 -= value;
		v2 -= value;
		v3 -= value;

		if (v1 < 0 || v2 < 0 || v3 < 0) {
			Debug.LogError ("Value of triangle index fell below 0 after subtracting");
		}
	}

	public void SwitchDirection() {
		int v1c = v1;
		int v3c = v3;

		v1 = v3c;
		v3 = v1c;
	}

	/*
	 * Transform a given list of triangles into an array (with 3 times as many elements).
	 */
	public static int[] GetTriangleArray(List<Triangle> triangles) {
		int[] triangleArray = new int[triangles.Count * 3];
		for (int i = 0; i < triangles.Count; i++) {
			Triangle triangle = triangles [i];
			triangleArray [i * 3    ] = triangle.v1;
			triangleArray [i * 3 + 1] = triangle.v2;
			triangleArray [i * 3 + 2] = triangle.v3;
		}
		return triangleArray;
	}

	public static List<Triangle> GetTriangleList(int[] triangles) {
		List<Triangle> triangleList = new List<Triangle> ();
		for (int i = 0; i < triangles.Length / 3; i++) {
			triangleList.Add (new Triangle (i * 3, i * 3 + 1, i * 3 + 2));
		}

		return triangleList;
	}
}
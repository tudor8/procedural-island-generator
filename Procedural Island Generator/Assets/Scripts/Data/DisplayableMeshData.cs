﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayableMeshData {
	[SerializeField] MeshData meshData;
	[SerializeField] Material material;

	public MeshData MeshData {
		get { return meshData; } set { meshData = value; }
	}

	public Material Material {
		get { return material; } set { material = value; }
	}

	public DisplayableMeshData(MeshData meshData, Material material) {
		this.meshData = meshData;
		this.material = material;
	}
}
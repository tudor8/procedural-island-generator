﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MapData {
	int width, height;
	float min, max;
	public float[,] data;

	public float Min    { get {return min   ;} set {min    = value;}}
	public float Max    { get {return max   ;} set {max    = value;}}
	public int   Width  { get {return width ;} set {width  = value;}}
	public int   Height { get {return height;} set {height = value;}}

	public MapData(int width, int height) {
		data = new float[width, height];
		min = float.MaxValue;
		max = float.MinValue;
		this.width = width;
		this.height = height;
	}

	public void Set(int x, int y, float value) {
		if (value < min)
			min = value;
		if (value > max)
			max = value;
		data [x, y] = value;
	}

	public float Get(int x, int y) {
		return data [x, y];
	}

	public float Get(Vector2Int pos) {
		return data [pos.x, pos.y];
	}

	public void Normalize() {
		float newMin = float.MaxValue;
		float newMax = float.MinValue;
		for (var x = 0; x < width; x++) {
			for (var y = 0; y < height; y++) {
				data[x, y] = Mathf.InverseLerp (min, max, data [x, y]);
				if (data[x, y] < newMin)
					newMin = data[x, y];
				if (data[x, y] > newMax)
					newMax = data[x, y];
			}
		}
		min = newMin;
		max = newMax;
	}

	public MapData CombineWith(MapData map) {
		MapData combinedMap = new MapData (width, height);
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				combinedMap.Set (x, y, Get (x, y) * map.Get (x, y));
			}
		}
		combinedMap.Normalize ();

		return combinedMap;
	}

	public MapData Add(MapData map, AnimationCurve curve) {
		MapData combinedMap = new MapData (width, height);
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				combinedMap.Set (x, y, Get (x, y) + curve.Evaluate (map.Get (x, y)) * map.Get (x, y));
			}
		}
		combinedMap.Normalize ();

		return combinedMap;
	}

	public MapData Subtract(MapData map, AnimationCurve curve, bool normalise, bool clamp) {
		MapData combinedMap = new MapData (width, height);
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				combinedMap.Set (x, y, Get (x, y) - curve.Evaluate (map.Get (x, y)) * map.Get (x, y));
				if (clamp) {
					combinedMap.Set (x, y, Mathf.Clamp(combinedMap.Get (x, y), 0f, 1f));
				}
			}
		}

		if (clamp) {
			combinedMap.min = float.MaxValue;
			combinedMap.max = float.MinValue;
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					combinedMap.Set (x, y, Mathf.Clamp(combinedMap.Get (x, y), 0f, 1f));
				}
			}
		}

		if(normalise)
			combinedMap.Normalize ();

		return combinedMap;
	}
}

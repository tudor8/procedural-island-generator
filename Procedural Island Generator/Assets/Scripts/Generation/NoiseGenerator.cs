﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseGenerator : MonoBehaviour {
	[SerializeField] bool randomStart;
	[SerializeField] Generator generator;
	[SerializeField] Vector3 offset;
	[SerializeField] float scale = 1f;
	[SerializeField][Range(0, 6)] public int octaves = 4;
	[SerializeField][Range(0.05f, 1.00f)] float persistance = 0.50f;
	[SerializeField][Range(0.05f, 5.00f)] float lacunarity  = 2.00f;

	public void OnValidate() {
		if (scale < 0.01f) { scale = 0.01f; }
		generator.OnValidate ();
	}

	public virtual float Get (float x, float y) {
		float amplitude = 1f;
		float frequency = 1f;
		float height    = 0f;

		for (int i = 0; i < octaves; i++) {
			x = x * frequency;
			y = y * frequency;

			float value = Mathf.PerlinNoise (x, y) * 2 - 1;
			height += value * amplitude;

			amplitude *= persistance;
			frequency *= lacunarity ;
		}

		return height;
	}

	public MapData GenerateNoiseMap(int width, int height, int seed, Vector3 offset) {
		MapData mapData = new MapData (width, height);

		Vector2[] octaveOffsets = new Vector2[octaves];
		if (randomStart) {
			System.Random randomSeed = new System.Random (seed);
			octaveOffsets = new Vector2[octaves];
			for (int i = 0; i < octaves; i++) {
				octaveOffsets [i] = new Vector2 (
					randomSeed.Next (-10000, 10000) + offset.x,
					randomSeed.Next (-10000, 10000) - offset.y
				);
			}
		}
		for (var y = 0; y < height; y++) {
			for (var x = 0; x < width; x++) {
				float amplitude   = 1f;
				float frequency   = 1f;
				float heightValue = 0f;

				for (int i = 0; i < octaves; i++) {
					float x1 = (x - width  / 2 + octaveOffsets[i].x) / scale * frequency;
					float y1 = (y - height / 2 + octaveOffsets[i].y) / scale * frequency;


					float value = Mathf.PerlinNoise (x1, y1) * 2 - 1;
					heightValue += value * amplitude;

					amplitude *= persistance;
					frequency *= lacunarity ;
				}

				mapData.Set (x, y, heightValue);
			}
		}	

		mapData.Normalize ();

		return mapData;
	}

	public MapData GenerateUniformMap(int width, int height) {
		MapData mapData = new MapData (width, height);
		float middle = height / 2;
		for (var x = 0; x < width; x++) {
			float value = Mathf.Abs (x - middle) / middle;
			for (var y = 0; y < height; y++) {
				mapData.Set (x, y, value);
			}
		}

		mapData.Normalize ();

		return mapData;
	}

	/**
	 * Generate an uniform map, starting at <startingValue> and ending at <endingValue>, in a square arond the the edges of the map.
	 * You can specify the percentage at which the value starts and ends
	 * Example: w = 5, h = 5, start = 0, end = 1, startValue = 1.0, endValue = 0.0
	 *  -------------------
	 * |1.0 1.0 1.0 1.0 1.0
	 * |1.0 0.5 0.5 0.5 1.0
	 * |1.0 0.5 0.0 0.5 1.0
	 * |1.0 0.5 0.5 0.5 1.0
	 * |1.0 1.0 1.0 1.0 1.0
	 *  -------------------
	 * 0.0        -> rangeStart = startingValue
	 * rangeStart -> rangeEnd   = linear value
	 * rangeEnd   -> 1.0        = endValue
	 */
	public MapData GenerateSquareUniformMap (Vector2Int size, float rangeStart, float rangeEnd, float startingValue, float endingValue) {
		MapData mapData = new MapData (size.x, size.y);

		int startingIndex = 0;
		int rangeStartIndex = (int) (Mathf.Min(size.x, size.y) * rangeStart / 2);
		int rangeEndIndex   = (int) (Mathf.Min(size.x, size.y) * rangeEnd   / 2);
		int endingIndex     = (int) (Mathf.Min(size.x, size.y) / 2);

		for (int i = startingIndex; i < rangeStartIndex; i++) {
			SetRectangleToValue (mapData, i, startingValue);
		}
		for (int i = rangeStartIndex; i < rangeEndIndex; i++) {
			SetRectangleToValue (mapData, i, Mathf.Lerp(startingValue, endingValue, (float)(i - rangeStartIndex) / (rangeEndIndex - rangeStartIndex)));
		}
		for (int i = rangeEndIndex; i < endingIndex; i++) {
			SetRectangleToValue (mapData, i, endingValue);
		}

		return mapData;
	}

	private void SetRectangleToValue(MapData mapData, int index, float value) {
		for(int i = index; i < mapData.Width - index; i++) {
			mapData.Set (i, index, value);
			mapData.Set (i, mapData.Height - index - 1, value);
		}
		for (int i= index; i < mapData.Height - index; i++) {
			mapData.Set (index, i, value);
			mapData.Set (mapData.Width - index - 1, i, value);
		}
	}
}

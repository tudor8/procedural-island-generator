﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class PoissonDiskAlgorithm : MonoBehaviour {
	public static readonly int VALID  = 0;
	public static readonly int ALREADY_PLACED = 1;
	public static readonly int INVALID  = 2;

	[SerializeField] Generator generator;
	[SerializeField] NoiseGenerator noiseGenerator;
	[SerializeField] int width;
	[SerializeField] int height;
	[SerializeField] int alreadyPlacedMinDistance = 3;

	[SerializeField] int pointCount;
	[SerializeField] int seed;
	[SerializeField] int maxIterations;
	[SerializeField] bool applyNoise;

	public void RandomiseSeed() {
		seed = Random.Range (0, 100000);
	}

	void OnValidate() {
		generator.OnValidate ();
	}

	public System.Random Generate(TreePlacer.IntArray[] initialState, out TreePlacer.IntArray[] finalState, out List<Vector2Int> positionsToSpawn) {
		EditorDestroyableObjectsCollector.Instance.DestroyAllChildren (transform);

		width  = initialState.GetLength (0);
		height = initialState[0].array.GetLength (0);

		System.Random random = new System.Random (seed);

		TreePlacer.IntArray[] grid = new TreePlacer.IntArray[initialState.GetLength(0)];
		for (int i = 0; i < initialState.GetLength(0); i++) {
			grid [i] = new TreePlacer.IntArray (initialState[0].array.GetLength(0));
			for (int j = 0; j < initialState[0].array.GetLength(0); j++) {
				grid [i].array [j] = initialState [i].array[j];
			}
		}

		List<Vector2> list = new List<Vector2> ();

		List<Vector2> availablePoints = new List<Vector2> ();
		UsefulMethods.ForEachElement (new Vector2Int (width, height), (position) => {
			if (grid [position.x].array[position.y] == VALID) {
				availablePoints.Add (position);
			}
		});

		Debug.Log (availablePoints.Count);

		int point = random.Next (availablePoints.Count);
		list.Add (availablePoints[point]);
		availablePoints.RemoveAt (point);

		positionsToSpawn = new List<Vector2Int> ();

		int currentIterations = 0;
		while (list.Count != 0 && currentIterations < maxIterations) {
			int index = random.Next(list.Count);
			Vector2 pos = list [index];
			list.RemoveAt (index);
			positionsToSpawn.Add (Vector2Int.FloorToInt(pos));

			for (int i = 0; i < pointCount; i++) {
				float angle    = (float)(random.NextDouble () * Mathf.PI * 2);
				float distance = (float)(random.NextDouble () * alreadyPlacedMinDistance + alreadyPlacedMinDistance);

				Vector2    newPos    = pos + distance * (new Vector2 (Mathf.Cos(angle), Mathf.Sin(angle)));
				Vector2Int newPosInt = new Vector2Int ((int)newPos.x, (int)newPos.y);

				bool tooClose = false;
				if(UsefulMethods.IsIndexValid (newPos.x, newPos.y, width, height)) {
					UsefulMethods.ForEachElementAroundPoint (newPosInt, alreadyPlacedMinDistance, (position) => {
						if (UsefulMethods.IsIndexValid (position.x, position.y, width, height)) {
							float pointDistance = Vector2Int.Distance (position, newPosInt);
							if (grid [position.x].array[position.y] == ALREADY_PLACED && pointDistance < alreadyPlacedMinDistance) {
								tooClose = true;
								return true;
							} 
						}
						return false;
					});
					if (!tooClose) {
						list.Add (newPos);
						grid [newPosInt.x].array[ newPosInt.y] = ALREADY_PLACED;
					}
				}
			}
			currentIterations++;
		}

		finalState = grid;

		return random;
	}
} 
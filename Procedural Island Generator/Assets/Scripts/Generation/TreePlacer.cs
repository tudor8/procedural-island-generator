﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TreeGenerator;
using System.Linq;

public class TreePlacer : MonoBehaviour {
	[System.Serializable]
	public class IntArray {
		public int[] array;

		public IntArray(int length) {
			array = new int[length];
		}
	}

	[System.Serializable]
	public class VectorArray {
		public Vector3[] array;

		public VectorArray(int length) {
			array = new Vector3[length];
		}
	}

	[SerializeField] GameObject treePrefab;
	[SerializeField] Generator generator;
	[SerializeField] NoiseGenerator noiseGenerator;
	[SerializeField] PoissonDiskAlgorithm poissonDiskAlgorithm;
	[SerializeField] DisplayableMapData colorMap;
	[SerializeField] Gradient leafColors;
	[SerializeField] Gradient branchColors;

	[SerializeField] int invalidMinDistance = 2;

	[SerializeField] List<TreeScript> trees;

	[SerializeField] int treeSeed;
	[SerializeField] IntArray[] initialState;
	[SerializeField][HideInInspector] VectorArray[] verticeReferences;
	[SerializeField][HideInInspector] List<Vector3> validPositions;

	[SerializeField] Transform testParent;
	[SerializeField] GameObject obj;
	[SerializeField] Material good;
	[SerializeField] Material bad;

	public void RandomiseSeed() {
		treeSeed = Random.Range (0, 100000);
	}

	public void RandomizeColourSeed() {
		colorMap.Seed = Random.Range (0, 100000);
	}

	public List<TreeScript> Trees {
		get { return trees; }
	}

	void OnValidate() {
		//generator.OnValidate ();
	}

	IntArray[] Transform(int [,] array) {
		IntArray[] arr = new IntArray[array.GetLength(0)];
		for (int i = 0; i < array.GetLength(0); i++) {
			arr [i] = new IntArray(array.GetLength(1));
			for (int j = 0; j < array.GetLength(1); j++) {
				arr [i].array [j] = array [i, j];
			}
		}

		return arr;
	}

	VectorArray[] Transform(Vector3 [,] array) {
		VectorArray[] arr = new VectorArray[array.GetLength(0)];
		for (int i = 0; i < array.GetLength(0); i++) {
			arr [i] = new VectorArray(array.GetLength(1));
			for (int j = 0; j < array.GetLength(1); j++) {
				arr [i].array [j] = array [i, j];
			}
		}

		return arr;
	}

	public void RandomzieTreeColours() {
		RandomizeColourSeed ();

		trees.Clear ();
		UsefulMethods.DestroyAllChildren (transform);

		PlaceDownTrees (validPositions);
	}

	public void RandomizeTreePositions() {
		trees.Clear ();
		poissonDiskAlgorithm.RandomiseSeed ();

		Generate (initialState, verticeReferences);
	}

	public void RandomizeTreeSeed() {
		RandomiseSeed ();

		trees.Clear ();
		UsefulMethods.DestroyAllChildren (transform);

		PlaceDownTrees (validPositions);
	}

	public void Generate(int[,] state, Vector3[,] vertices) {
		this.initialState = Transform(state);
		this.verticeReferences = Transform(vertices);

		Generate (initialState, verticeReferences);
	}

	public void Generate(IntArray[] state, VectorArray[] vertices) {
		UsefulMethods.DestroyAllChildren (transform);
		UsefulMethods.DestroyAllChildren (testParent);

		/*
		for (int i = 0; i < initialState.GetLength(0); i++) {
			for (int j = 0; j < initialState.GetLength(1); j++) {
				GameObject o = Instantiate (obj, new Vector3 (), Quaternion.identity);
				o.transform.position = new Vector3 (i, 0, j);
				o.transform.SetParent (testParent);
				if (initialState [i, j] == 0) {
					o.GetComponent<Renderer> ().sharedMaterial = good;
				} 
				else {
					o.GetComponent<Renderer> ().sharedMaterial = bad;
				}
			}
		}
		*/

		trees.Clear ();
		IntArray[] finalState;
		List<Vector2Int> positions;

		poissonDiskAlgorithm.Generate (initialState, out finalState, out positions);

		Debug.Log (positions.Count);

		validPositions = IndexToRealPositions (positions, finalState, this.verticeReferences);

		Debug.Log (validPositions.Count);

		PlaceDownTrees (validPositions);
	}

	public List<Vector3> IndexToRealPositions(List<Vector2Int> positions, IntArray[] tileStates, VectorArray[] vertices) {
		List<Vector3> validPositions = new List<Vector3> ();
		for (int i = 0; i < positions.Count; i++) {
			bool tooClose = false;
				
			UsefulMethods.ForEachElementAroundPoint (positions [i], invalidMinDistance, (position) => {
				if (UsefulMethods.IsIndexValid (position.x, position.y, generator.TotalTiles.x, generator.TotalTiles.y)) {
					if (tileStates [position.x].array[position.y] == 2 && Vector2Int.Distance (position, positions[i]) < invalidMinDistance) {
						tooClose = true;
						return true;
					}
				}
				return false;
			});

			if (!tooClose) {
				Vector3 vertex = vertices[positions[i].x].array[positions[i].y];
				Vector3 position = new Vector3 (positions[i].x, vertex.y - 0.2f, positions[i].y);
				validPositions.Add (position);
			} 
		}
		return validPositions;
	}

	public void PlaceDownTrees(List<Vector3> positions) {
		System.Random random = new System.Random (treeSeed);

		Vector2 offset = UsefulMethods.GetOffset (generator.TotalTiles);

		colorMap.MapData = noiseGenerator.GenerateNoiseMap (generator.ActualSize.x, generator.ActualSize.y, colorMap.Seed, colorMap.Offset);

		for (int i = 0; i < positions.Count; i++) {
			float value = colorMap.MapData.Get ((int)positions [i].x, (int)positions [i].y);
			Vector3 position = new Vector3 (positions [i].x - offset.x, positions [i].y, positions [i].z - offset.y);
			PlaceDownTree (random, position, branchColors.Evaluate (value), leafColors.Evaluate (value), i); 
		}
	}

	public void PlaceDownTree(System.Random random, Vector3 position, Color branchColor, Color leafColor, int index) {
		GameObject tree = Instantiate (treePrefab, position, Quaternion.identity) as GameObject;
		tree.name = "Tree" + (index + 1);

		TreeScript treeScript = tree.GetComponent<TreeScript> ();

		treeScript.Seed = random.Next ();

		GradientColorKey[] gCKeys = treeScript.ColorGradient.colorKeys;
		GradientAlphaKey[] gAKeys = treeScript.ColorGradient.alphaKeys;
		gCKeys [0].color = branchColor;
		gCKeys [1].color = branchColor;
		gCKeys [2].color = leafColor;
		treeScript.ColorGradient.SetKeys (gCKeys, gAKeys);
		treeScript.GenerateNewTexture ();

		tree.transform.SetParent (this.transform, true);

		treeScript.Generate ();

		trees.Add (treeScript);
	}
}
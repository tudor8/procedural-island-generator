﻿using UnityEngine;
  

public class TextureGenerator : MonoBehaviour {		
	[SerializeField] Generator generator;
	[SerializeField] TextureWrapMode wrapMode   = TextureWrapMode.Clamp;
	[SerializeField] FilterMode      filterMode = FilterMode     .Point;

	void OnValidate() {
		generator.OnValidate();
	}

	public Texture2D GenerateTexture (int width, int height, Color[] pixels) {
		Texture2D texture = new Texture2D(width, height);
		texture.name = "colorPallete";
		texture.SetPixels (pixels);
		texture.filterMode = filterMode;
		texture.wrapMode   = wrapMode  ;
		texture.Apply();

		return texture;
	}

	public Color[] GenerateColorPallete(Region[] regions) {
		Color[] colors  = new Color[100];

		int regionSpace = 100 / regions.Length;
		for (int i = 0; i < regions.Length; i++) {
			for(int j = 0; j < regionSpace; j++) {
				colors [j + i * regionSpace] = regions [i].color;
			}
		}

		return colors;
	}

	public Vector2 GetUVCoordsFromPallete(Region[] regions, int index) {
		return new Vector2 ((float)index / regions.Length, 0);
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class FloodAlgorithm : MonoBehaviour {
	public static readonly int LAND  = 0;
	public static readonly int WATER = 1;
	public static readonly int EDGE  = 2;

	int[,] originalValues;
	int[,] newValues     ;

	Queue<Vector2Int> nodes = new Queue<Vector2Int> ();

	public bool IsIndexValid(Vector2Int position) {
		return UsefulMethods.IsIndexValid (position.x, position.y, newValues.GetLength (0), newValues.GetLength (1));
	}

	private bool IsTrue(Vector2Int position) {
		return originalValues [position.x, position.y] == 1;
	} 

	private bool NotVisitedYet(Vector2Int position) {
		return newValues [position.x, position.y] != 1;
	}

	private void AddNodeToQueue(Queue<Vector2Int> queue, Vector2Int node) {
		queue.Enqueue (node);
		newValues [node.x, node.y] = 1;
	}

	/**
	 * Given a 2D array with values having the following meanings:
	 * 0 = land
	 * 1 = water
	 * 2 = edge
	 * Transform it by flood filling from the edges until it hits land, marking the edges of the land.
	 * Essentially marking any water that touches the edges as 1, the connections to land as 2 and the inside of the land as 3.
	 * Example:
	 * 1 1 1 1 1 1  ->  1 1 1 1 1 1
	 * 1 0 0 0 0 1      1 2 2 2 2 1
	 * 1 0 1 1 0 1      1 2 0 0 2 1
	 * 1 0 1 0 0 0      1 2 0 0 0 2
	 * 0 0 0 0 0 0      2 0 0 0 0 0
	 */
	public int[,] ApplyFloodAlgorithm(int[,] values, Vector2Int startingLocation) {
		Queue<Vector2Int> nodes = new Queue<Vector2Int> ();
		this.originalValues = values;
		newValues = new int[values.GetLength(0), values.GetLength(1)];

		if (values [startingLocation.x, startingLocation.y] == WATER) {
			nodes.Enqueue (startingLocation);
		}

		while (nodes.Count != 0) {
			Vector2Int node = nodes.Dequeue ();
			Vector2Int up    = node + Vector2Int.up   ;
			Vector2Int left  = node + Vector2Int.left ; 
			Vector2Int right = node + Vector2Int.right;
			Vector2Int down  = node + Vector2Int.down ;
			if (IsIndexValid (up) && NotVisitedYet(up)) {
				if (IsTrue (up)) {
					AddNodeToQueue (nodes, up);
				} 
				else {
					newValues [up.x, up.y] = EDGE;
				}
			}
			if (IsIndexValid (left) && NotVisitedYet(left)) {
				if (IsTrue (left)) {
					AddNodeToQueue(nodes, left);
				} 
				else {
					newValues [left.x, left.y] = EDGE;
				}
			}
			if (IsIndexValid (right) && NotVisitedYet(right)) {
				if (IsTrue (right)) {
					AddNodeToQueue(nodes, right);
				} 
				else {
					newValues [right.x, right.y] = EDGE;
				}
			}
			if (IsIndexValid (down) && NotVisitedYet(down)) {
				if (IsTrue (down)) {
					AddNodeToQueue(nodes, down);
				} 
				else {
					newValues [down.x, down.y] = EDGE;
				}
			}
		}

		return newValues;
	}
}
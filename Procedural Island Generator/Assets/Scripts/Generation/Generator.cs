﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine;
using System.Linq;

[ExecuteInEditMode]
public class Generator : MonoBehaviour {
	public enum TextureType      { Texture, Colors }
	public enum VerticeDirection { Top, Bottom}

	private static readonly string ChunkFormat = "Terrain Chunk [{0}][{1}]";

	//------------------------------------------------------------------------------------------------------
	// Inspector Fields
	//------------------------------------------------------------------------------------------------------
	[SerializeField] NoiseGenerator noiseGenerator;
	[SerializeField] TextureGenerator textureGenerator;
	[SerializeField] MeshGenerator terrainGenerator;
	[SerializeField] MeshTransformer meshTransformer;
	[SerializeField] TreePlacer treePlacer;
	[SerializeField] GameObject topChunksParent;
	[SerializeField] GameObject bottomChunksParent;

	[SerializeField] Material colorMaterial;
	[SerializeField] Material meshMaterial;
	[SerializeField] GameObject chunkPrefab;
	[SerializeField] GameObject testObject; // Can be spawned for testing
	[SerializeField] GameObject treeObject;

	[SerializeField] TextureType textureType = TextureType.Texture; // Whether to use UVs or colors
	[SerializeField] bool autoUpdate      = false; // Turn this off/on to make the editor update when changes are made in the inspector
	[SerializeField] bool getRidOfWater   = true ; // Will remove water from the edges (but not inside water)
	[SerializeField] bool smoothDiagonals = true ;
	[SerializeField] bool smoothEdges     = true ;
	[SerializeField] bool applyFallout    = false; // Will make the edges water
	[SerializeField] bool extrudeHeights  = true ; // Will make the mesh 3D rather than a 2D plane
	[SerializeField] bool addTrees        = false; // Will add trees randomly on any grass tiles
	[SerializeField] bool createTop       = false; // Will generate the top part of the island
	[SerializeField] bool createBottom    = false; // Will generate the bottom part of the island
	[SerializeField] bool original        = false; // Will not convert triangles to make the mesh look nicer
	[SerializeField] float gap = 1f; // The gap between the top and bottom chunks
	[SerializeField] Vector2Int tilesPerChunk = new Vector2Int(50, 50); // Size of one chunk
	[SerializeField] Vector2Int chunkSize     = new Vector2Int( 1,  1); // How many chunks to generate

	[SerializeField] string topPalleteName = "colorPalleteTop.png";
	[SerializeField] string botPalleteName = "colorPalleteBot.png";
	[SerializeField] string dataName       = "data.json";
	[SerializeField] string exportPath     = "/export/";

	[SerializeField] DisplayableMapData heightMap ;
	[SerializeField] DisplayableMapData bottomMap ;
	[SerializeField] DisplayableMapData falloutMap;

	Dictionary<String, TerrainChunk> topTerrainChunks    = new Dictionary<string, TerrainChunk>();
	Dictionary<String, TerrainChunk> bottomTerrainChunks = new Dictionary<string, TerrainChunk>();
	Vector2Int totalTiles; 
	Vector2Int actualSize;

	public Vector2Int TilesPerChunk {
		get { return tilesPerChunk; }
	}

	public Vector2Int ChunkSize {
		get { return chunkSize; }
	}

	public Vector2Int TotalTiles {
		get { return totalTiles; }
	}

	public Vector2Int ActualSize {
		get { return totalTiles; }
	}

	public bool Original {
		get { return original; }
	}

	public bool ExtrudeHeights {
		get { return extrudeHeights; }
	}

	public bool SmoothDiagonals {
		get { return smoothDiagonals; }
	}

	public bool SmoothEdges {
		get { return smoothEdges; }
	}

	public float Gap {
		get { return gap; }
	}

	public GameObject TestObject {
		get { return testObject; }
	}

	public class JSONFormat {
		public List<JSONWrapper> topChunks;
		public List<JSONWrapper> botChunks;
		public int numberOfTrees;

		public JSONFormat() {
			topChunks = new List<JSONWrapper>();
			botChunks = new List<JSONWrapper>();
		}
	}
		
	public class MeshList {
		public List<JSONWrapper> meshes;

		public MeshList() {
			meshes = new List<JSONWrapper>();
		}
	}
		
	//------------------------------------------------------------------------------------------------------
	// Generation
	//------------------------------------------------------------------------------------------------------
	public void OnValidate() {
		tilesPerChunk.x = Mathf.Clamp (tilesPerChunk.x, 2, 100);
		tilesPerChunk.y = Mathf.Clamp (tilesPerChunk.y, 2, 100);
		chunkSize.x = Mathf.Clamp (chunkSize.x, 1, 10);
		chunkSize.y = Mathf.Clamp (chunkSize.y, 1, 10);
		if (autoUpdate) {
			GenerateIsland ();
		}
	}

	public void Randomize() {
		heightMap.Seed = UnityEngine.Random.Range (0, 100000);
		bottomMap.Seed = heightMap.Seed;
	}

	public static string GetChunkName(int x, int y) {
		return string.Format (ChunkFormat, x, y);
	}

	public TerrainChunk GetChunk(int x, int y, VerticeDirection direction) {
		switch (direction) {
		case VerticeDirection.Top:
			return topTerrainChunks    [GetChunkName (x, y)];
		case VerticeDirection.Bottom:
			return bottomTerrainChunks [GetChunkName (x, y)];
		}
		return null;
	}

	//------------------------------------------------------------------------------------------------------
	// Export the mesh data and textures
	//------------------------------------------------------------------------------------------------------
	public void Export() {
		string topTexturePath = Application.dataPath + exportPath + topPalleteName;
		File.WriteAllBytes (topTexturePath, (topTerrainChunks    [GetChunkName(0, 0)].MeshRenderer.sharedMaterial.mainTexture as Texture2D).EncodeToJPG());

		string botTexturePath = Application.dataPath + exportPath + botPalleteName;
		File.WriteAllBytes (botTexturePath, (bottomTerrainChunks [GetChunkName(0, 0)].MeshRenderer.sharedMaterial.mainTexture as Texture2D).EncodeToJPG());

		JSONFormat jsonFormat = new JSONFormat ();

		UsefulMethods.ForEachElement (chunkSize, (position) => {
			TerrainChunk chunkTop = topTerrainChunks    [GetChunkName(position.x, position.y)];
			TerrainChunk chunkBot = bottomTerrainChunks [GetChunkName(position.x, position.y)];
			jsonFormat.topChunks.Add(JSONWrapper.FromMesh(chunkTop.MeshFilter, topPalleteName));
			jsonFormat.botChunks.Add(JSONWrapper.FromMesh(chunkBot.MeshFilter, botPalleteName));
		});
		jsonFormat.numberOfTrees = treePlacer.Trees.Count;

		string file = Application.dataPath + exportPath + dataName;
		File.WriteAllText (file, JsonUtility.ToJson (jsonFormat));

		for (int i = 0; i < treePlacer.Trees.Count; i++) {
			MeshList tree = new MeshList ();
			for (int j = 0; j < treePlacer.Trees [i].meshFilters.Count; j++) {
				tree.meshes.Add(JSONWrapper.FromMesh(treePlacer.Trees [i].meshFilters[j], treePlacer.Trees [i].gameObject.name + ".png"));
			}
			string texturePath = Application.dataPath + exportPath + treePlacer.Trees [i].gameObject.name;
			File.WriteAllBytes (texturePath + ".png", (treePlacer.Trees [i].meshRenderers[0].sharedMaterial.mainTexture as Texture2D).EncodeToPNG());
			File.WriteAllText (texturePath + ".json", JsonUtility.ToJson (tree));
		} 

		Debug.Log ("finished saving");
	}

	//------------------------------------------------------------------------------------------------------
	// Generation
	//------------------------------------------------------------------------------------------------------
	/**
	 * The main method that gets called and makes sure everything is fine
	 * It will generate a fallout map, the top and bottom maps, get rid of water and then add trees.
	 */
	public void GenerateIsland() {
		totalTiles = UsefulMethods.MultiplyVectors (tilesPerChunk, chunkSize);
		actualSize = totalTiles + Vector2Int.one;

		UsefulMethods.DestroyAllChildren (topChunksParent   .transform);
		UsefulMethods.DestroyAllChildren (bottomChunksParent.transform);

		if (applyFallout) { falloutMap.MapData = noiseGenerator.GenerateSquareUniformMap (actualSize, rangeStart:0.0f, rangeEnd:0.3f, startingValue:1.0f, endingValue:0.0f); }

		// Generate Top Chunks
		if(createTop) {
			heightMap.MapData = noiseGenerator.GenerateNoiseMap (actualSize.x, actualSize.y, heightMap.Seed, heightMap.Offset);
			if (applyFallout) {
				heightMap.MapData = heightMap.MapData.Subtract (falloutMap.MapData, falloutMap.Curve, false, true);
			} 

			MapDataExtra heightExtraInfo = GenerateChunks (topTerrainChunks, topChunksParent, VerticeDirection.Top, heightMap);

			if (getRidOfWater) {
				meshTransformer.RemoveDeepVertices (transform.position, topTerrainChunks, heightExtraInfo, heightMap, VerticeDirection.Top);
			}

			if (addTrees) {
				int[,] availableSpots = new int[totalTiles.x, totalTiles.y];
				UsefulMethods.ForEachElement(totalTiles, (position) => {
					availableSpots [position.x, position.y] = 
						(heightMap.MapData.Get (position.x, position.y)  > 0.5 && 
						 heightMap.MapData.Get (position.x, position.y) <= 0.7) 
						? 0 : 2;
				});

				Vector3[,] vertices = new Vector3[heightExtraInfo.verticeReferences.GetLength (0), heightExtraInfo.verticeReferences.GetLength (1)];
				for (int i = 0; i < heightExtraInfo.verticeReferences.GetLength(0); i++) {
					for (int j = 0; j < heightExtraInfo.verticeReferences.GetLength(1); j++) {
						Dictionary<int, MeshData> v = heightExtraInfo.verticeReferences [i, j];
						if (v.Count > 0) {
							KeyValuePair<int, MeshData> pair = v.First ();
							vertices [i, j] = pair.Value.verticeList [pair.Key];
						} 
						else {
							vertices [i, j] = new Vector3 ();
						}
					}
				}
					
				treePlacer.Generate (availableSpots, vertices);
			}
		}

		// Generate Bottom Chunks
		if (createBottom) {
			bottomMap.MapData = noiseGenerator.GenerateNoiseMap (actualSize.x, actualSize.y, heightMap.Seed, heightMap.Offset);
			if (applyFallout) {
				bottomMap.MapData = bottomMap.MapData.Subtract (falloutMap.MapData, falloutMap.Curve, false, true);
			}

			MapDataExtra bottomExtraInfo = GenerateChunks (bottomTerrainChunks, bottomChunksParent, VerticeDirection.Bottom, bottomMap);

			if (getRidOfWater) {
				meshTransformer.RemoveDeepVertices (transform.position, bottomTerrainChunks, bottomExtraInfo, bottomMap, VerticeDirection.Bottom);
				meshTransformer.ReverseDirection (VerticeDirection.Bottom);
			}
		}
	}

	/**
	 * The process of creating many chunks at once and storing them in a dictionary.
	 */
	public MapDataExtra GenerateChunks(Dictionary<String, TerrainChunk> terrainChunks, GameObject chunkParent, VerticeDirection direction, DisplayableMapData mapData) {
		terrainChunks.Clear ();
		EditorDestroyableObjectsCollector.Instance.DestroyAllChildren (chunkParent.transform);

		Vector2 offset = UsefulMethods.GetOffset (UsefulMethods.MultiplyVectors(tilesPerChunk, (chunkSize - Vector2Int.one)));

		MapDataExtra mapDataExtra = new MapDataExtra (mapData.MapData, totalTiles);

		UsefulMethods.ForEachElement (chunkSize, (chunkPosition) => {
			String chunkName = GetChunkName(chunkPosition.x, chunkPosition.y);

			Vector3 position = new Vector3 (chunkPosition.x * tilesPerChunk.x - offset.x, 0, chunkPosition.y * tilesPerChunk.y - offset.y);

			DisplayableMeshData meshInfo = GenerateChunk (chunkPosition, direction, mapData, mapDataExtra.tileTypes, mapDataExtra.verticeReferences);

			GameObject terrainChunk = Instantiate (chunkPrefab, new Vector3(), Quaternion.identity) as GameObject;
			terrainChunks [chunkName] = terrainChunk.GetComponent<TerrainChunk> ();
			terrainChunk.transform.SetParent (chunkParent.transform);
			terrainChunk.transform.localPosition = position;
			terrainChunk.name = chunkName; 
			terrainChunks [chunkName].Initialize (meshInfo);

			mapDataExtra.SetUpChunk (meshInfo.MeshData.ToMesh(), UsefulMethods.MultiplyVectors(chunkPosition, tilesPerChunk), tilesPerChunk, mapData.Regions);
		});

		return mapDataExtra;
	}

	/**
	 * The process of creating a single chunk and its texture.
	 */
	public DisplayableMeshData GenerateChunk(Vector2Int chunkCoords, VerticeDirection direction, DisplayableMapData mapData, string[,] tileTypes, Dictionary<int, MeshData>[,] verticeReferences) {
		MeshData meshData = null;
		Material material = null;

		meshData = terrainGenerator.GenerateSimpleMesh  (
			mapData, 
			UsefulMethods.MultiplyVectors(chunkCoords, tilesPerChunk),
			tileTypes, 
			verticeReferences
		);

		Color[] colors = textureGenerator.GenerateColorPallete (mapData.Regions);

		if     (textureType == TextureType.Texture) { 
			material = new Material(meshMaterial );
			material.mainTexture = textureGenerator.GenerateTexture (100, 1, colors);
		}
		else if(textureType == TextureType.Colors ) {
			material = new Material(colorMaterial);
			material.mainTexture = null;
		}

		return new DisplayableMeshData(meshData, material);
	}
}
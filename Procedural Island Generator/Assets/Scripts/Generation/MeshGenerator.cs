﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshGenerator : MonoBehaviour {
	[SerializeField] Generator        generator       ;
	[SerializeField] TextureGenerator textureGenerator;
	/**
	 * TLDR: Generates the base mesh and decides which the diagonals need to be and what colors the triangles should be based on the below logic.
	 * In order for the final mesh to look nice, we must swap triangle colors around depending on adjacent tiles
	 * Step 1:
	 * 	Any tiles with 2 adjacent tiles of a higher tier will have the triangle next to those tiles become that tier
	 * 
	 * Step 2
	 *  Any tiles with 2 adjacent tiles 
	 */
	public MeshData GenerateSimpleMesh (
		DisplayableMapData regionWrapper,
		Vector2Int mapOffset,
		string[,] tileTypes, 
		Dictionary<int, MeshData>[,] verticeReferences
	) {
		MapData        mapData  = regionWrapper.MapData;
		Region[]       regions  = regionWrapper.Regions;
		AnimationCurve curve    = regionWrapper.Curve;
		float          modifier = regionWrapper.HeightModifier;

		MeshData meshData = new MeshData ();

		Vector2 offset = UsefulMethods.GetOffset (generator.TilesPerChunk);

		UsefulMethods.ForEachElement (generator.TilesPerChunk, (position) => {
			Vector2Int offsetPosition = position + mapOffset;

			Vector2Int topLeft     = offsetPosition + Vector2Int.down + Vector2Int.left;
			Vector2Int top         = offsetPosition + Vector2Int.down;
			Vector2Int topRight    = offsetPosition + Vector2Int.down + Vector2Int.right;
			Vector2Int middleLeft  = offsetPosition + Vector2Int.left;
			Vector2Int middleRight = offsetPosition + Vector2Int.right;
			Vector2Int bottomLeft  = offsetPosition + Vector2Int.up + Vector2Int.left;
			Vector2Int bottom      = offsetPosition + Vector2Int.up;
			Vector2Int bottomRight = offsetPosition + Vector2Int.up + Vector2Int.right;

			float topLeftHeight  = mapData.Get (offsetPosition) * curve.Evaluate (mapData.Get (offsetPosition)) * modifier;
			float topRightHeight = mapData.Get (middleRight   ) * curve.Evaluate (mapData.Get (middleRight   )) * modifier;
			float botLeftHeight  = mapData.Get (bottom        ) * curve.Evaluate (mapData.Get (bottom        )) * modifier;
			float botRightHeight = mapData.Get (bottomRight   ) * curve.Evaluate (mapData.Get (bottomRight   )) * modifier;

			int topLeftIndex     = GetColorIndex (mapData, regions, topLeft       );
			int topMiddleIndex   = GetColorIndex (mapData, regions, top           );
			int topRightIndex    = GetColorIndex (mapData, regions, topRight      );
			int middleLeftIndex  = GetColorIndex (mapData, regions, middleLeft    );
			int middleIndex      = GetColorIndex (mapData, regions, offsetPosition);
			int middleRightIndex = GetColorIndex (mapData, regions, middleRight   );
			int botLeftIndex     = GetColorIndex (mapData, regions, bottomLeft    );
			int botMiddleIndex   = GetColorIndex (mapData, regions, bottom        );
			int botRightIndex    = GetColorIndex (mapData, regions, bottomRight   );

			bool changeDirection = false;

			// Indexes refer to which color to use
			int triangle1Index = middleIndex;
			int triangle2Index = middleIndex;

			tileTypes [offsetPosition.x, offsetPosition.y] = TileTypes.NoChange.ToString ();

			// On a water tile
			if (!generator.Original) {
				int currentColorIndex = GetColorIndex (mapData, regions, offsetPosition);

				int prevColorInLine = currentColorIndex - 1;
				int nextColorInLine = currentColorIndex + 1;

				bool noChanges = true;
				if (nextColorInLine == regions.Length) {
					noChanges = false;
				}

				// Step 1
				if (nextColorInLine != regions.Length) {
					int neighbours = 
						UsefulMethods.BoolToInt(IsTileTheRightColor (currentColorIndex, topMiddleIndex  ))+
						UsefulMethods.BoolToInt(IsTileTheRightColor (currentColorIndex, middleLeftIndex )) +
						UsefulMethods.BoolToInt(IsTileTheRightColor (currentColorIndex, middleRightIndex)) +
						UsefulMethods.BoolToInt(IsTileTheRightColor (currentColorIndex, botMiddleIndex  ));

					// With 0 or 1 neighbours, the upper color takes dominance over the current color
					if (neighbours == 1 || neighbours == 0) {
						triangle1Index = nextColorInLine;
						triangle2Index = nextColorInLine;
						tileTypes [offsetPosition.x, offsetPosition.y] = TileTypes.Filled.ToString ();
					}
					// Bottom Right
					else if (
						!IsTileTheRightColor (currentColorIndex, middleRightIndex) &&
						!IsTileTheRightColor (currentColorIndex, botMiddleIndex  )
					) {
						changeDirection = true;
						triangle1Index = currentColorIndex;
						triangle2Index = nextColorInLine;
						tileTypes [offsetPosition.x, offsetPosition.y] = TileTypes.BotRight.ToString ();
					}
					// Bottom Left
					else if (
						!IsTileTheRightColor (currentColorIndex, middleLeftIndex) &&
						!IsTileTheRightColor (currentColorIndex, botMiddleIndex )
					) {
						triangle1Index = nextColorInLine;
						triangle2Index = currentColorIndex;
						tileTypes [offsetPosition.x, offsetPosition.y] = TileTypes.BotLeft.ToString ();
					}
					// Top Right
					else if (
						!IsTileTheRightColor (currentColorIndex, topMiddleIndex  ) &&
						!IsTileTheRightColor (currentColorIndex, middleRightIndex)
					) {
						triangle1Index = currentColorIndex;
						triangle2Index = nextColorInLine;
						tileTypes [offsetPosition.x, offsetPosition.y] = TileTypes.TopRight.ToString ();
					}
					// Top Left
					else if (
						!IsTileTheRightColor (currentColorIndex, middleLeftIndex) &&
						!IsTileTheRightColor (currentColorIndex, topMiddleIndex )
					) {
						changeDirection = true;
						triangle1Index = nextColorInLine;
						triangle2Index = currentColorIndex;
						tileTypes [offsetPosition.x, offsetPosition.y] = TileTypes.TopLeft.ToString ();
					} 
					else {
						noChanges = true;
					}
				}

				// Step 2
				if (prevColorInLine != -1 && noChanges) {
					// Bottom Left
					if (
						!IsTileTheRightColor (prevColorInLine, topMiddleIndex  ) &&
					    !IsTileTheRightColor (prevColorInLine, middleRightIndex) &&
					     IsTileTheRightColor (prevColorInLine, topLeftIndex    ) &&
					     IsTileTheRightColor (prevColorInLine, botRightIndex   ) &&
					     IsTileTheRightColor (prevColorInLine, botMiddleIndex  ) &&
					     IsTileTheRightColor (prevColorInLine, middleLeftIndex )
					) {
						triangle1Index = prevColorInLine;
						triangle2Index = currentColorIndex;
						tileTypes [offsetPosition.x, offsetPosition.y] = TileTypes.CornerBotLeft.ToString ();
					}
					// Bottom Right 
					else if (
					   !IsTileTheRightColor (prevColorInLine, topMiddleIndex  ) &&
					   !IsTileTheRightColor (prevColorInLine, middleLeftIndex ) &&
						IsTileTheRightColor (prevColorInLine, middleRightIndex) &&
						IsTileTheRightColor (prevColorInLine, botMiddleIndex  ) &&
						IsTileTheRightColor (prevColorInLine, botLeftIndex    ) &&
						IsTileTheRightColor (prevColorInLine, topRightIndex   )
					) {
						changeDirection = true;
						triangle1Index = currentColorIndex;
						triangle2Index = prevColorInLine;
						tileTypes [offsetPosition.x, offsetPosition.y] = TileTypes.CornerBotRight.ToString ();
					}
					// Top Left
					else if (
					   !IsTileTheRightColor (prevColorInLine, botMiddleIndex  ) &&
					   !IsTileTheRightColor (prevColorInLine, middleRightIndex) &&
						IsTileTheRightColor (prevColorInLine, middleLeftIndex ) &&
						IsTileTheRightColor (prevColorInLine, topMiddleIndex  ) &&
						IsTileTheRightColor (prevColorInLine, botLeftIndex    ) &&
						IsTileTheRightColor (prevColorInLine, topRightIndex   )
					) {
						changeDirection = true;
						triangle1Index = prevColorInLine;
						triangle2Index = currentColorIndex;
						tileTypes [offsetPosition.x, offsetPosition.y] = TileTypes.CornerTopLeft.ToString ();
					}
					// Top Right
					else if (
					   !IsTileTheRightColor (prevColorInLine, botMiddleIndex  ) &&
					   !IsTileTheRightColor (prevColorInLine, middleLeftIndex ) &&
						IsTileTheRightColor (prevColorInLine, middleRightIndex) &&
						IsTileTheRightColor (prevColorInLine, topMiddleIndex  ) &&
						IsTileTheRightColor (prevColorInLine, botRightIndex   ) &&
						IsTileTheRightColor (prevColorInLine, topLeftIndex    )
					) {
						triangle1Index = currentColorIndex;
						triangle2Index = prevColorInLine;
						tileTypes [offsetPosition.x, offsetPosition.y] = TileTypes.CornerTopRight.ToString ();
					}

				}
			}
				 
			if (changeDirection) {
				// Top Left Square
				AddNewTriangle (meshData, verticeReferences, regions, offset, triangle1Index, mapOffset,
					new Vector3 (position.x, botLeftHeight , position.y) + Vector3.forward,
					new Vector3 (position.x, topRightHeight, position.y) + Vector3.right,
					new Vector3 (position.x, topLeftHeight , position.y)
				);
				AddNewTriangle (meshData, verticeReferences, regions, offset, triangle2Index, mapOffset,
					new Vector3 (position.x, botRightHeight, position.y) + Vector3.forward + Vector3.right,
					new Vector3 (position.x, topRightHeight, position.y) + Vector3.right,
					new Vector3 (position.x, botLeftHeight , position.y) + Vector3.forward
				);
			} 
			else {
				// Top Left Square
				AddNewTriangle (meshData, verticeReferences, regions, offset, triangle1Index, mapOffset,
					new Vector3 (position.x, botLeftHeight , position.y) + Vector3.forward,
					new Vector3 (position.x, botRightHeight, position.y) + Vector3.forward + Vector3.right,
					new Vector3 (position.x, topLeftHeight , position.y)
				);
				AddNewTriangle (meshData, verticeReferences, regions, offset, triangle2Index, mapOffset,
					new Vector3 (position.x, botRightHeight, position.y) + Vector3.forward + Vector3.right,
					new Vector3 (position.x, topRightHeight, position.y) + Vector3.right,
					new Vector3 (position.x, topLeftHeight , position.y)
				);
			}
		});

		return meshData;
	}

	/**
	 * A tile counts as the right color if is less than or equal to the current color.
	 * But what does that mean?
	 * Let's look at an example:
	 *   ! IsTileTheRightColor (currentColorIndex, middleRightIndex) &&
	 *   ! IsTileTheRightColor (currentColorIndex, botMiddleIndex  )  
	 * In this case we check whether the tiles at middleRight and bottomMiddle are of a higher tier
	 */
	private bool IsTileTheRightColor(int expected, int current) {
		return current <= expected;
	}

	/**
	 * Get the index for a given coordinate in the map system
	 * Returns -1 if the coordinates are out of bounds
	 */
	private int GetColorIndex(MapData mapData, Region[] regions, Vector2Int position) {
		if(!UsefulMethods.IsIndexValid(position.x, position.y, mapData.Width, mapData.Height) )
			return -1;

		return Region.GetCorrespondingIndex (mapData.Get (position), regions);
	}

	/**
	 * Add vertices, colors, uvs and triangles to the mesh data.
	 * 
	 */
	private void AddNewTriangle(
		MeshData meshData, 
		Dictionary<int, MeshData>[,] verticeReferences, 
		Region[] regions, 
		Vector2 offset,
		int colorIndex,
		Vector2 mapOffset,
		Vector3 pos1, 
		Vector3 pos2, 
		Vector3 pos3
	) {
		if (!generator.ExtrudeHeights) { pos1.y = pos2.y = pos3.y = 0;}

		meshData.verticeList.Add (new Vector3(pos1.x - offset.x, pos1.y, pos1.z - offset.y));
		meshData.verticeList.Add (new Vector3(pos2.x - offset.x, pos2.y, pos2.z - offset.y));
		meshData.verticeList.Add (new Vector3(pos3.x - offset.x, pos3.y, pos3.z - offset.y));

		meshData.colorList.Add (regions[colorIndex].color);
		meshData.colorList.Add (regions[colorIndex].color);
		meshData.colorList.Add (regions[colorIndex].color);

		meshData.uvsList.Add(textureGenerator.GetUVCoordsFromPallete(regions, colorIndex));
		meshData.uvsList.Add(textureGenerator.GetUVCoordsFromPallete(regions, colorIndex));
		meshData.uvsList.Add(textureGenerator.GetUVCoordsFromPallete(regions, colorIndex));

		Triangle triangle = new Triangle (meshData.verticeList.Count - 3, meshData.verticeList.Count - 2, meshData.verticeList.Count - 1);

		meshData.triangleList.Add(triangle);
		 
		verticeReferences [Mathf.RoundToInt(pos1.x + mapOffset.x), Mathf.RoundToInt(pos1.z + mapOffset.y)][triangle.v1] = meshData;
		verticeReferences [Mathf.RoundToInt(pos2.x + mapOffset.x), Mathf.RoundToInt(pos2.z + mapOffset.y)][triangle.v2] = meshData;
		verticeReferences [Mathf.RoundToInt(pos3.x + mapOffset.x), Mathf.RoundToInt(pos3.z + mapOffset.y)][triangle.v3] = meshData;
	}
}
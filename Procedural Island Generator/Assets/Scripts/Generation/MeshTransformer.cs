﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshTransformer : MonoBehaviour {
	[SerializeField] Generator generator;
	[SerializeField] FloodAlgorithm floodAlgorithm;

	public int[,] RemoveDeepVertices(
		Vector3 center, 
		Dictionary<string, TerrainChunk> terrainChunks, 
		MapDataExtra mapDataExtra,
		DisplayableMapData info, 
		Generator.VerticeDirection direction
	) {
		int[,] tilesThatShouldBeRemoved = new int[generator.TotalTiles.x, generator.TotalTiles.y];

		List<MeshData>[,] triangleReferences = new List<MeshData>[generator.TotalTiles.x, generator.TotalTiles.y];
		UsefulMethods.ForEachElement(generator.TotalTiles, (position) => {
			triangleReferences [position.x, position.y] = new List<MeshData>();
		});

		Vector2 verticeOffset = UsefulMethods.GetOffset (generator.TilesPerChunk);

		// We are going to eventually delete vertices, after which we need to update the map data
		Dictionary<int, MeshData>[,] verticeReferences = new Dictionary<int, MeshData>[generator.ActualSize.x, generator.ActualSize.y];
		UsefulMethods.ForEachElement(generator.ActualSize, (position) => {
			verticeReferences [position.x, position.y] = new Dictionary<int, MeshData>();
		});
			
		// Mark all water tiles
		float minSize = float.MaxValue;
		UsefulMethods.ForEachElement (generator.TotalTiles, (position) => {
			float value = mapDataExtra.tiles [position.x, position.y].value;
			float actualValue = value * info.Curve.Evaluate (value) * info.HeightModifier;

			if (value < 0.4) {
				tilesThatShouldBeRemoved [position.x, position.y] = FloodAlgorithm.WATER;

			}
			else if (actualValue < minSize) {
				minSize = actualValue;
			}	
		});
		minSize -= generator.Gap;

		// The flood algorithm will mark all water tiles connected to the outside as 1, the land edges as 2 and the land as 0
		tilesThatShouldBeRemoved = floodAlgorithm.ApplyFloodAlgorithm (tilesThatShouldBeRemoved, new Vector2Int ());

		UsefulMethods.ForEachElement (generator.ChunkSize, (position) => {
			TerrainChunk chunk = generator.GetChunk(position.x, position.y, direction);

			for(int i = 0; i < chunk.MeshData.verticeList.Count; i++) {
				Vector3 vertex = chunk.MeshData.verticeList [i];
				if (generator.ExtrudeHeights) {
					vertex.y -= 0.7f;
				}

				if (direction == Generator.VerticeDirection.Bottom) {
					vertex.y *= -1;
				}
				chunk.MeshData.verticeList [i] = vertex;
			}
		});

		// The first step is to lower down all the vertices belonging to edges
		if(generator.SmoothEdges) {
			UsefulMethods.ForEachElement (generator.ChunkSize, (chunkPosition) => {
				Vector2Int offset = UsefulMethods.MultiplyVectors(generator.TilesPerChunk, chunkPosition);

				TerrainChunk chunk = generator.GetChunk(chunkPosition.x, chunkPosition.y, direction);

				UsefulMethods.ForEachElement(generator.TilesPerChunk, (position) => {
					Vector2Int coords = position + offset;

					List<Triangle> triangles = mapDataExtra.tiles [coords.x, coords.y].triangles;
					bool special = false;
					int index1 = -1;
					int index2 = -1;
					// Not all water tiles need to be removed. Some water tiles act as corners for the landmass
					if (tilesThatShouldBeRemoved [coords.x, coords.y] == FloodAlgorithm.WATER) {
						if (!mapDataExtra.tileTypes [coords.x, coords.y].Equals (TileTypes.NoChange.ToString ()) &&
							(mapDataExtra.tiles [coords.x, coords.y].value >= 0.2)) {
							special = true;
						}
					} 
					else if (tilesThatShouldBeRemoved [coords.x, coords.y] == FloodAlgorithm.EDGE) {
						int numberOfNeighbours = GetNumberOfWaterNeighbours(tilesThatShouldBeRemoved, mapDataExtra.tileTypes, coords);

						// For a tile to be valid for being lowered, it must not be a corner and it must not have 3 or more water neighbours
						if (!mapDataExtra.tileTypes [coords.x, coords.y].StartsWith ("Corner")) {
							/* Here we set the vertices of straight tiles
							 * Example (right neighbour is a water tile)
							 * 1.1 ---- 0.5 <- Needs to be lowered
							 *  |  Edge  |
							 *  |  Tile  |
							 * 1.2 ---- 0.2 <- Needs to be lowered
							 */
							if (numberOfNeighbours < 3 && mapDataExtra.tileTypes[coords.x, coords.y].Equals(TileTypes.NoChange.ToString())) {
								// Bottom
								if (IsNeighbourAWaterTile (tilesThatShouldBeRemoved, mapDataExtra.tileTypes, coords + Vector2Int.up)) {
									index1 = triangles[0].v1;
									index2 = triangles[1].v1;
								}
								// Top
								if (IsNeighbourAWaterTile (tilesThatShouldBeRemoved, mapDataExtra.tileTypes, coords + Vector2Int.down)) {
									index1 = triangles[0].v3;
									index2 = triangles[1].v2;
								}
								// Right
								if (IsNeighbourAWaterTile (tilesThatShouldBeRemoved, mapDataExtra.tileTypes, coords + Vector2Int.right)) {
									index1 = triangles[1].v2;
									index2 = triangles[1].v1;
								}
								// Left
								if (IsNeighbourAWaterTile (tilesThatShouldBeRemoved, mapDataExtra.tileTypes, coords + Vector2Int.left)) {
									index1 = triangles[0].v3;
									index2 = triangles[0].v1;
								}
							}
						} 
						else {
							if (numberOfNeighbours == 2) {
								if (mapDataExtra.tileTypes [coords.x, coords.y] == TileTypes.CornerTopLeft.ToString ()) {
									index1 = triangles[0].v3;
									index2 = triangles[0].v1;
								} 
								else if (mapDataExtra.tileTypes [coords.x, coords.y] == TileTypes.CornerBotLeft.ToString ()) {
									index1 = triangles[0].v3;
									index2 = triangles[0].v2;
								} 
								else if (mapDataExtra.tileTypes [coords.x, coords.y] == TileTypes.CornerTopRight.ToString ()) {
									index1 = triangles[0].v3;
									index2 = triangles[0].v2;
								} 
								else if (mapDataExtra.tileTypes [coords.x, coords.y] == TileTypes.CornerBotRight.ToString ()) {
									index1 = triangles[0].v2;
									index2 = triangles[0].v1;
								}
							}
						}
					}	
					// Found a tile that is half/half, so we must lower one of the triangles
					if (special) {
						if      (mapDataExtra.tileTypes [coords.x, coords.y] == TileTypes.BotLeft.ToString ()) {
							index1 = triangles[0].v3;
							index2 = triangles[0].v2;
						} 
						else if (mapDataExtra.tileTypes [coords.x, coords.y] == TileTypes.TopLeft.ToString ()) {
							index1 = triangles[0].v2;
							index2 = triangles[0].v1;
						} 
						else if (mapDataExtra.tileTypes [coords.x, coords.y] == TileTypes.BotRight.ToString ()) {
							index1 = triangles[1].v3;
							index2 = triangles[1].v2;
						}
						else if (mapDataExtra.tileTypes [coords.x, coords.y] == TileTypes.TopRight.ToString ()) {
							index1 = triangles[1].v3;
							index2 = triangles[1].v1;
						}
					}

					if(index1 != -1) {
						SetAllVerticesToHeight (mapDataExtra, GetVerticeFromTriangleIndex(chunk.MeshData, index1), 0, verticeOffset, offset);
					}
					if(index2 != -1) {
						SetAllVerticesToHeight (mapDataExtra, GetVerticeFromTriangleIndex(chunk.MeshData, index2), 0, verticeOffset, offset);
					}
				});
			});
		}

		// With all tiles lowered as they should be, we can move on to adding them to the new mesh, removing bad tiles
		UsefulMethods.ForEachElement(generator.ChunkSize, (chunkPosition) => { 
			TerrainChunk chunk = generator.GetChunk(chunkPosition.x, chunkPosition.y, direction);

			MeshData newMeshData = new MeshData ();

			int index = 0;
			int offset = 0;
			Vector2Int coordsOffset = UsefulMethods.MultiplyVectors(generator.TilesPerChunk, chunkPosition);
			UsefulMethods.ForEachElement(generator.TilesPerChunk, (position) => {
				Vector2Int coords = position + coordsOffset;

				List<Triangle> triangles = mapDataExtra.tiles [coords.x, coords.y].triangles;
				bool special = false;
				bool filled  = false;
				// Check what we need to do with a water tile
				if (tilesThatShouldBeRemoved[coords.x, coords.y] == FloodAlgorithm.WATER) {
					// Deep Water, which is always removed 
					if (mapDataExtra.tiles [coords.x, coords.y].value < 0.2) { 
						offset += 3 * triangles.Count; triangles.Clear ();
					}
					// Normal Water that didn't change
					else if (mapDataExtra.tileTypes [coords.x, coords.y].Equals (TileTypes.NoChange.ToString ())) {
						offset += 3 * triangles.Count; triangles.Clear ();
					}
					// Water corners are always removed
					else if (mapDataExtra.tileTypes [coords.x, coords.y].StartsWith("Corner")) {
						offset += 3 * triangles.Count; triangles.Clear ();
					}
					// Filled water tiles have been transformed to the next tier, so they act as if they are land tiles
					else if (mapDataExtra.tileTypes [coords.x, coords.y].Equals (TileTypes.Filled.ToString   ())) {
						filled = true;
					}
					// Found a half half triangle
					else {
						special = true;
					}
				} 
				// Check what we need to do with a land tile
				else if(tilesThatShouldBeRemoved[coords.x, coords.y] == FloodAlgorithm.EDGE || filled) {
					int numberOfNeighbours = GetNumberOfWaterNeighbours(tilesThatShouldBeRemoved, mapDataExtra.tileTypes, coords);
					if (!mapDataExtra.tileTypes [coords.x, coords.y].StartsWith ("Corner")) {
						// Remove all land tiles with 3 or more water neighbours
						if(numberOfNeighbours >= 3) {
							offset += 3 * triangles.Count; triangles.Clear ();
						}
					} 
					else if(numberOfNeighbours == 2) { // Corner blocks
						if(UsefulMethods.StringMatchesAnyOfTheParameters(
							mapDataExtra.tileTypes [coords.x, coords.y], 
							TileTypes.CornerBotLeft.ToString (), 
							TileTypes.CornerTopLeft.ToString ())
						) {
							offset += 3;
							triangles.RemoveAt (0);
						}
						else if(UsefulMethods.StringMatchesAnyOfTheParameters(
							mapDataExtra.tileTypes [coords.x, coords.y], 
							TileTypes.CornerBotRight.ToString (), 
							TileTypes.CornerTopRight.ToString ())
						) {
							offset += 3;
							triangles.RemoveAt (1);
						}
					}
				}	
				// Found a tile that is half/half, so we must remove the bad triangles
				if (special) {
					if(UsefulMethods.StringMatchesAnyOfTheParameters(
						mapDataExtra.tileTypes [coords.x, coords.y], 
						TileTypes.BotLeft.ToString (), 
						TileTypes.TopLeft.ToString ())
					) {
						offset += 3;
						triangles.RemoveAt (1);
					}
					else if(UsefulMethods.StringMatchesAnyOfTheParameters(
						mapDataExtra.tileTypes [coords.x, coords.y], 
						TileTypes.BotRight.ToString (), 
						TileTypes.TopRight.ToString ())
					) {
						offset += 3;
						triangles.RemoveAt (0);
					}
				}

				// Go through each of the triangles left and add them
				for (int i = 0; i < triangles.Count; i++) {
					Triangle triangle = triangles [i];
					newMeshData.verticeList.Add(chunk.MeshData.verticeList [triangle.v1]);
					newMeshData.verticeList.Add(chunk.MeshData.verticeList [triangle.v2]);
					newMeshData.verticeList.Add(chunk.MeshData.verticeList [triangle.v3]);

					newMeshData.uvsList.Add(chunk.MeshData.uvsList [triangle.v1]);
					newMeshData.uvsList.Add(chunk.MeshData.uvsList [triangle.v2]);
					newMeshData.uvsList.Add(chunk.MeshData.uvsList [triangle.v3]);

					if (chunk.MeshData.colorList.Count != 0) {
						newMeshData.colorList.Add(chunk.MeshData.colorList [triangle.v1]);
						newMeshData.colorList.Add(chunk.MeshData.colorList [triangle.v2]);
						newMeshData.colorList.Add(chunk.MeshData.colorList [triangle.v3]);
					}

					newMeshData.triangleList.Add(new Triangle(index * 3, index * 3 + 1, index * 3 + 2));

					Vector2 v1Pos = new Vector2 (chunk.MeshData.verticeList [triangle.v1].x, chunk.MeshData.verticeList [triangle.v1].z);
					Vector2 v2Pos = new Vector2 (chunk.MeshData.verticeList [triangle.v2].x, chunk.MeshData.verticeList [triangle.v2].z);
					Vector2 v3Pos = new Vector2 (chunk.MeshData.verticeList [triangle.v3].x, chunk.MeshData.verticeList [triangle.v3].z);
					UsefulMethods.ReverseOffset (ref v1Pos, verticeOffset);
					UsefulMethods.ReverseOffset (ref v2Pos, verticeOffset);
					UsefulMethods.ReverseOffset (ref v3Pos, verticeOffset);
					verticeReferences [Mathf.RoundToInt (v1Pos.x + coordsOffset.x), Mathf.RoundToInt (v1Pos.y + coordsOffset.y)][index * 3    ] = newMeshData;
					verticeReferences [Mathf.RoundToInt (v2Pos.x + coordsOffset.x), Mathf.RoundToInt (v2Pos.y + coordsOffset.y)][index * 3 + 1] = newMeshData;
					verticeReferences [Mathf.RoundToInt (v3Pos.x + coordsOffset.x), Mathf.RoundToInt (v3Pos.y + coordsOffset.y)][index * 3 + 2] = newMeshData;

					triangles [i] = new Triangle (index * 3, index * 3 + 1, index * 3 + 2);

					triangleReferences[coords.x, coords.y].Add(newMeshData); 

					index++;
				}
				string s = "";
				foreach(Triangle t in triangles) {
					s += "[" + t.v1 + " " + t.v2 + " " + t.v3 + "]";
				}
				//Debug.Log("MeshData contains [" + newMeshData.triangleList.Count + "]  Current Triangle " + s);
				mapDataExtra.tiles [coords.x, coords.y].triangles = triangles;

			});
			if (chunk.MeshData.colorList.Count != 0) {
				newMeshData.colors = null;
			}
			chunk.MeshFilter.sharedMesh = newMeshData.ToMesh ();
			chunk.MeshData = newMeshData;
		});
		mapDataExtra.verticeReferences = verticeReferences;

		if (generator.SmoothDiagonals) {
			SmoothOutDiagonals (direction, mapDataExtra, tilesThatShouldBeRemoved, triangleReferences);
		}

		return tilesThatShouldBeRemoved;
	}

	public void SmoothOutDiagonals(Generator.VerticeDirection direction, MapDataExtra mapDataExtra, int[,] tilesThatShouldBeRemoved, List<MeshData>[,] triangleReferences) {
		UsefulMethods.ForEachElement (generator.ChunkSize, (chunkPosition) => {
			TerrainChunk chunk = generator.GetChunk(chunkPosition.x, chunkPosition.y, direction);

			Vector2Int coordsOffset = UsefulMethods.MultiplyVectors(generator.TilesPerChunk, chunkPosition);

			UsefulMethods.ForEachElement(generator.TilesPerChunk, (position) => {
				Vector2Int coords = position + coordsOffset;

				bool found = false;
				if (tilesThatShouldBeRemoved[coords.x, coords.y] == FloodAlgorithm.WATER) {
					// Water tiles that are of the following types: TopLeft, TopRight, BotLeft, BotRight
					if (!(mapDataExtra.tiles [coords.x, coords.y].value < 0.2) && 
						UsefulMethods.StringStartsWithAnyOfTheParameters(mapDataExtra.tileTypes [coords.x, coords.y], "Top", "Bot")
					){
						found = true;							
					}
				} 
				else if (tilesThatShouldBeRemoved[coords.x, coords.y] == FloodAlgorithm.EDGE && mapDataExtra.tileTypes [coords.x, coords.y].StartsWith("Corner")) {
					found = true;	
				}

				if (found) {
					string type = mapDataExtra.tileTypes [coords.x, coords.y];
					bool hasTileChanged = TileTypesHelper.IsTileChanged (type);
					Vector2Int neighbour1Coords = new Vector2Int(-1, -1);
					Vector2Int neighbour2Coords = new Vector2Int(-1, -1);
					if      (
						mapDataExtra.tileTypes [coords.x, coords.y] == TileTypes.BotLeft        .ToString () || 
						mapDataExtra.tileTypes [coords.x, coords.y] == TileTypes.CornerTopRight .ToString ()
					) {
						neighbour1Coords = coords + Vector2Int.right;
						neighbour2Coords = coords + Vector2Int.down ;
					} 
					else if (
						mapDataExtra.tileTypes [coords.x, coords.y] == TileTypes.TopLeft        .ToString () ||
						mapDataExtra.tileTypes [coords.x, coords.y] == TileTypes.CornerBotRight .ToString ()
					) {
						neighbour1Coords = coords + Vector2Int.left;
						neighbour2Coords = coords + Vector2Int.down;
					}
					else if (
						mapDataExtra.tileTypes [coords.x, coords.y] == TileTypes.BotRight     .ToString () || 
						mapDataExtra.tileTypes [coords.x, coords.y] == TileTypes.CornerTopLeft.ToString ()
					) {
						neighbour1Coords = coords + Vector2Int.right;
						neighbour2Coords = coords + Vector2Int.up;
					}
					else if (
						mapDataExtra.tileTypes [coords.x, coords.y] == TileTypes.TopRight     .ToString () || 
						mapDataExtra.tileTypes [coords.x, coords.y] == TileTypes.CornerBotLeft.ToString ()
					) {
						neighbour1Coords = coords + Vector2Int.right;
						neighbour2Coords = coords + Vector2Int.down;
					}

					if(UsefulMethods.IsIndexValid(neighbour1Coords.x, neighbour1Coords.y, generator.TotalTiles.x, generator.TotalTiles.y)) {
						CheckForDiagonalTriangles(mapDataExtra, chunk.MeshData, coords, neighbour1Coords, triangleReferences);
					}

					if(UsefulMethods.IsIndexValid(neighbour2Coords.x, neighbour2Coords.y, generator.TotalTiles.x, generator.TotalTiles.y)) {
						CheckForDiagonalTriangles(mapDataExtra, chunk.MeshData, coords, neighbour2Coords, triangleReferences);
					}
				}
			});
			chunk.MeshFilter.mesh = chunk.MeshData.ToMesh ();
		});
	}

	private void CheckForDiagonalTriangles(MapDataExtra mapDataExtra, MeshData meshData, Vector2Int coords, Vector2Int neighbourCoords, List<MeshData>[,] triangleReferences) {
		string type = mapDataExtra.tileTypes [coords.x, coords.y];
		bool hasTileChanged = TileTypesHelper.IsTileChanged (type);

		MapDataExtra.Tile neighbourTile = mapDataExtra.tiles [neighbourCoords.x, neighbourCoords.y];
		string neighbourType = mapDataExtra.tileTypes [neighbourCoords.x, neighbourCoords.y];
		bool hasNeighbourChanged = TileTypesHelper.IsTileChanged (neighbourType);
		if (hasTileChanged != hasNeighbourChanged && neighbourTile.triangles.Count == 2) {
			Triangle triangle1 = neighbourTile.triangles [0];
			MeshData triangl1MeshData = triangleReferences [neighbourCoords.x, neighbourCoords.y] [0];
			Triangle triangle2 = neighbourTile.triangles [1];
			MeshData triangl2MeshData = triangleReferences [neighbourCoords.x, neighbourCoords.y] [1];

			int t1v1 = triangle1.v1;
			int t1v2 = triangle1.v2;
			int t1v3 = triangle1.v3;
			int t2v1 = triangle2.v1;
			int t2v2 = triangle2.v2;
			int t2v3 = triangle2.v3;

			int triangle1Index = t1v1 / 3;
			int triangle2Index = t2v1 / 3;

			//Debug.Log (meshData.triangleList.Count + " " + triangle1Index + " " + triangle2Index);
			if (hasNeighbourChanged) {
				triangl1MeshData.triangleList [triangle1Index].v1 = t1v1;
				triangl1MeshData.triangleList [triangle1Index].v2 = t2v1;
				triangl1MeshData.triangleList [triangle1Index].v3 = t1v3;

				triangl2MeshData.triangleList [triangle2Index].v1 = t1v3;
				triangl2MeshData.triangleList [triangle2Index].v2 = t2v2;
				triangl2MeshData.triangleList [triangle2Index].v3 = t2v3;
			}
			else{
				triangl1MeshData.triangleList [triangle1Index].v1 = t1v1;
				triangl1MeshData.triangleList [triangle1Index].v2 = t2v2;
				triangl1MeshData.triangleList [triangle1Index].v3 = t1v3;

				triangl2MeshData.triangleList [triangle2Index].v1 = t2v1;
				triangl2MeshData.triangleList [triangle2Index].v2 = t2v2;
				triangl2MeshData.triangleList [triangle2Index].v3 = t1v1;

				//GameObject o1 = GameObject.Instantiate (generator.TestObject, meshData.vertices [meshData.triangles [triangle2.v1]], Quaternion.identity) as GameObject; o1.name = "1";
				//GameObject o2 = GameObject.Instantiate (generator.TestObject, meshData.vertices [meshData.triangles [triangle2.v2]], Quaternion.identity) as GameObject; o2.name = "2";
				//GameObject o3 = GameObject.Instantiate (generator.TestObject, meshData.vertices [meshData.triangles [triangle2.v3]], Quaternion.identity) as GameObject; o3.name = "3";
			}
		}
	}

	public void ReverseDirection(Generator.VerticeDirection direction) {
		UsefulMethods.ForEachElement(generator.ChunkSize, (chunkPosition) => {
			TerrainChunk chunk = generator.GetChunk(chunkPosition.x, chunkPosition.y, direction);

			MeshData meshData = MeshData.FromMesh (chunk.MeshFilter.sharedMesh);

			for(int i = 0; i < meshData.triangles.Length / 3; i++) {
				int v1 = meshData.triangles [i * 3    ];
				int v3 = meshData.triangles [i * 3 + 2];

				meshData.triangles [i * 3 + 2] = v1;
				meshData.triangles [i * 3    ] = v3;
			}

			chunk.MeshFilter.mesh = meshData.ToMesh ();
		});
	}

	private void SetAllVerticesToHeight(MapDataExtra mapDataExtra, Vector2 position, float height, Vector2 offset, Vector2 mapOffset) {
		UsefulMethods.ReverseOffset (ref position, offset);
		//Debug.Log (position);
		foreach (KeyValuePair<int, MeshData> i in mapDataExtra.verticeReferences[(int)(position.x + mapOffset.x), (int)(position.y + mapOffset.y)]) {
			//Debug.Log (i.Value + " " + i.Key);
			Vector3 pos = i.Value.verticeList [i.Key];
			pos.y = height;
			i.Value.verticeList [i.Key] = pos;
		}
	}

	private Vector2 GetVerticeFromTriangleIndex(MeshData meshData, int index) {
		return new Vector2 (meshData.verticeList [index].x, meshData.verticeList [index].z);
	}

	private int GetNumberOfWaterNeighbours(int[,] tilesThatShouldBeRemoved, string[,] tileTypes, Vector2Int coords) {
		int numberOfNeighbours = 0;
		UsefulMethods.ForEachNeighbour(coords, (position) => {
			numberOfNeighbours += UsefulMethods.BoolToInt (IsNeighbourAWaterTile (tilesThatShouldBeRemoved, tileTypes, position));
		});
		return numberOfNeighbours;
	}

	private static bool IsTileTheRightType(string[,] tileTypes, int xCoord, int yCoord, string type) {
		if (!UsefulMethods.IsIndexValid (xCoord, yCoord, tileTypes.GetLength (0), tileTypes.GetLength (1))) {return false;}

		return tileTypes [xCoord, yCoord].Equals (type);
	}

	private static bool IsTileDeepWater(MapData mapData, int xCoord, int yCoord) {
		if (!UsefulMethods.IsIndexValid (xCoord, yCoord, mapData.data.GetLength (0), mapData.data.GetLength (1))) {return false;}

		return mapData.data [xCoord, yCoord] <= 0.2;
	}

	private static bool IsNeighbourAWaterTile(int[,] tilesThatShouldBeRemoved, Vector2Int coords) {
		if (!UsefulMethods.IsIndexValid (coords.x, coords.y, tilesThatShouldBeRemoved.GetLength (0), tilesThatShouldBeRemoved.GetLength (1))) {return false;}
		return tilesThatShouldBeRemoved [coords.x, coords.y] == 1;
	}

	private static bool IsNeighbourAWaterTile(int[,] tilesThatShouldBeRemoved, string[,] tileTypes, Vector2Int coords) {
		if (!UsefulMethods.IsIndexValid (coords.x, coords.y, tilesThatShouldBeRemoved.GetLength (0), tilesThatShouldBeRemoved.GetLength (1))) {return false;}
		return tilesThatShouldBeRemoved [coords.x, coords.y] == 1 && tileTypes[coords.x, coords.y].Equals("NoChange");
	}
}
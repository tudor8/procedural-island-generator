﻿using System;

public enum TileTypes {
	NoChange,
	Filled,
	TopLeft,
	TopRight,
	BotLeft,
	BotRight,
	CornerTopLeft,
	CornerTopRight,
	CornerBotLeft,
	CornerBotRight
}
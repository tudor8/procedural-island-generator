﻿using System;

public class TileTypesHelper {
	public static bool IsTileChanged(TileTypes type) {
		return 
			type == TileTypes.BotRight       ||
			type == TileTypes.TopLeft        ||
			type == TileTypes.CornerBotRight ||
			type == TileTypes.CornerTopLeft;
	}

	public static bool IsTileChanged(string type) {
		return IsTileChanged ((TileTypes)System.Enum.Parse (typeof(TileTypes), type));
	}

}
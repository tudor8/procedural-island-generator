﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Temp
{
	public enum BiomeType { 
		Tundra, 
		Boreal, 
		TropicalForest, 
		Forest, 
		Desert , // Barren area, with close to no water
		Savanna, // Savannas are deserts, with a good amount of vegetation
		PolarDesert, // 
		Water 
	}
	[System.Serializable]
	public class Biome {
		public BiomeType name;
		public Color grassColor;
		public Color waterColor;
	}

	[System.Serializable]
	public class BiomeTable {
		[Range(1, 10)] public int rowCount;
		[Range(1, 10)] public int colCount;
		public string[] heatNames     = { "test" };
		public string[] moistureNames = { "test" };
		[System.Serializable]
		public class BiomeRow {
			public BiomeType[] row;
		}

		public BiomeRow[] rows = new BiomeRow[7];

		public BiomeType Get(int x, int y) {
			return rows [x].row [y];
		}

		public void Update(string[] heatNames, string[] moistureNames) {
			this.heatNames = heatNames;
			this.moistureNames = moistureNames;
		}
	}
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class EditorDestroyableObjectsCollector : Singleton<EditorDestroyableObjectsCollector> {
	List<GameObject> objects = new List<GameObject>();

	void Update() {
		if (Instance == null) {
			Debug.Log ("yes");
			InitiateSingleton ();
		}

		for (int i = 0; i < objects.Count; i++) {
			DestroyImmediate (objects[i]);
		}
	}

	public void DestroyObjects(List<GameObject> objects) {
		this.objects.AddRange (objects);
	}

	public void DestroyAllChildren(Transform target) {
		for(int i = transform.childCount - 1; i >= 0; i--) {
			UnityEditor.EditorApplication.delayCall+=()=> {
				DestroyImmediate(transform.GetChild(0).gameObject);
			};
		}
	}

	public void CleanUp() {
		for (int i = 0; i < objects.Count; i++) {
			Debug.Log ("clean up");
			DestroyImmediate (objects[i]);
		}
	}

	public void Initialise() {
		InitiateSingleton ();
	}
}
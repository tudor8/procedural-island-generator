﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;


public class UsefulMethods {
	public static Vector2Int[] GetNeighbours() {
		return new Vector2Int[] { Vector2Int.up, Vector2Int.right, Vector2Int.down, Vector2Int.left };
	}

	public static int BoolToInt(bool value) {
		return value ? 1 : 0;
	}

	public static bool IsIndexValid(int xCoord, int yCoord, int xSize, int ySize) {
		return 
			xCoord >= 0 && xCoord < xSize &&
			yCoord >= 0 && yCoord < ySize;
	}

	public static bool IsIndexValid(float xCoord, float yCoord, float xSize, float ySize) {
		return 
			xCoord >= 0 && xCoord < xSize &&
			yCoord >= 0 && yCoord < ySize;
	}

	public static Vector2 GetOffset(Vector2 size) {
		return new Vector2 (size.x / 2f, size.y / 2f);
	}

	public static void ReverseOffset (ref Vector2 position, Vector2 offset) {
		position += offset;
	}

	public static Vector3 MultiplyVectors(Vector3 v1, Vector3 v2) {
		return new Vector3 (v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
	}

	public static Vector2 MultiplyVectors(Vector2 v1, Vector2 v2) {
		return new Vector2 (v1.x * v2.x, v1.y * v2.y);
	}

	public static Vector2Int MultiplyVectors(Vector2Int v1, Vector2Int v2) {
		return new Vector2Int (v1.x * v2.x, v1.y * v2.y);
	}

	public static bool StringMatchesAnyOfTheParameters (string paramToCheck, params string[] otherParams) {
		for (int i = 0; i < otherParams.Length; i++) {
			if (paramToCheck.Equals(otherParams[i])) {
				return true;
			}
		}
		return false;
	}

	public static bool StringStartsWithAnyOfTheParameters (string paramToCheck, params string[] otherParams) {
		for (int i = 0; i < otherParams.Length; i++) {
			if (paramToCheck.StartsWith(otherParams[i])) {
				return true;
			}
		}
		return false;
	}

	public delegate bool BreakableAction(Vector2Int position);
	public delegate void Action(Vector2Int position);
	public static void ForEachElement(Vector2 size, Action action) {
		for(int y = 0; y < size.y; y++) {
			for(int x = 0; x < size.x; x++) {
				action (new Vector2Int (x, y)); 
			}
		}
	}

	public static void ForEachNeighbour(Vector2Int position, Action action) {
		Vector2Int[] neighbours = UsefulMethods.GetNeighbours ();
		for (int i = 0; i < neighbours.Length; i++) {
			action (neighbours [i]);
		}
	}

	public static void ForEachElementAroundPoint(Vector2Int point, int distance, BreakableAction action) { 
		for(int y = point.y - distance; y < point.y + distance; y++) {
			for(int x = point.x - distance; x < point.x + distance; x++) {
				if (action (new Vector2Int (x, y)))
					break;
			}
		}
	}

	public static void DestroyAllChildren(Transform target) {
		for(int i = target.childCount - 1; i >= 0; i--) {
			UnityEditor.EditorApplication.delayCall+=()=> {
				GameObject.DestroyImmediate(target.GetChild(0).gameObject);
			};
		}
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TreePlacer))]
public class TreePlacerEditor : Editor {
	public override void OnInspectorGUI() {
		TreePlacer script = (TreePlacer)target;

		DrawDefaultInspector ();
		if (GUILayout.Button ("Randomize Tree Positions")) {
			script.RandomizeTreePositions ();
		}
		if (GUILayout.Button ("Randomize Tree Colours")) {
			script.RandomzieTreeColours ();
		}
		if (GUILayout.Button ("Randomize Tree Shapes")) {
			script.RandomizeTreeSeed ();
		}
	}
}

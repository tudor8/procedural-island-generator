﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace CustomPropertyDrawers {
	[SerializeField]
	public class ReadOnly : PropertyAttribute {}
	[CustomPropertyDrawer(typeof(ReadOnly))]
	public class ReadOnlyDrawer : PropertyDrawer {
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
			GUI.enabled = false;
			EditorGUI.PropertyField (position, property, label, true);
			GUI.enabled = true;
		}
	}

	[CustomPropertyDrawer ( typeof (Region))]
	public class RegionDrawer : PropertyDrawer {
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
			Helper.drawGUI (position, property, label, new Helper.LabelPair[] { 
				new Helper.LabelPair("name"  , "Name:"  , 0.25f), 
				new Helper.LabelPair("height", "Height:", 0.20f),
				new Helper.LabelPair("color" , "Color:" , 0.55f)
			});
		}
	}

	[CustomPropertyDrawer (typeof(Temp.BiomeTable))]
	public class BiomeTable : PropertyDrawer {
		float height = 18f;
		bool showDetails = true;
		int rows;
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
			property.isExpanded = EditorGUI.Foldout (position, property.isExpanded, label, true);
			position.height = height;

			EditorGUI.BeginProperty (position, label, property);

			if (property.isExpanded) {
				SerializedProperty heatTypes     = property.FindPropertyRelative ("heatNames"    );
				SerializedProperty moistureTypes = property.FindPropertyRelative ("moistureNames");
				float width = position.width / (moistureTypes.arraySize + 1);
				// Top left corner
				position.y += height;

				Rect contentPos = position;
				contentPos.height = height;
				contentPos.width  = width ;
				if (heatTypes.arraySize != 0 && moistureTypes.arraySize != 0) {
					rows = moistureTypes.arraySize + 1;
					EditorGUI.indentLevel++;
					SerializedProperty data = property.FindPropertyRelative ("rows");

					if (data.arraySize != moistureTypes.arraySize) {
						data.arraySize = moistureTypes.arraySize;
					}

					// Draw the top labels
					contentPos.x = position.x + width;
					contentPos.y = position.y;
					for (int i = 0; i < heatTypes.arraySize; i++) {
						EditorGUI.LabelField (contentPos, heatTypes.GetArrayElementAtIndex (i).stringValue);
						contentPos.x += contentPos.width;
					}

					// Draw the left labels
					contentPos.x = position.x;
					contentPos.y = position.y + height;
					for (int i = 0; i < moistureTypes.arraySize; i++) {
						EditorGUI.LabelField (contentPos, moistureTypes.GetArrayElementAtIndex (i).stringValue);
						contentPos.y += height;
					}

					// The actual table
					contentPos.y = position.y + height;
					contentPos.x = position.x + width ;
					for (int j = 0; j < moistureTypes.arraySize; j++) {
						SerializedProperty row = data.GetArrayElementAtIndex (j).FindPropertyRelative ("row");
						// Validate the data array
						if (row.arraySize != heatTypes.arraySize) { row.arraySize = heatTypes.arraySize; }

						for (int i = 0; i < heatTypes.arraySize; i++) {
							EditorGUI.PropertyField (contentPos, row.GetArrayElementAtIndex (i), GUIContent.none);
							contentPos.x += width;
						}

						// Next Row
						contentPos.x = position.x + width;
						contentPos.y += height;
					}
					EditorGUI.indentLevel--;
				} 
				else {
					rows = 0;
				}
			}
			else {
				rows = 0;
			}
			EditorGUI.EndProperty (); 
		}

		public override float GetPropertyHeight (SerializedProperty property, GUIContent label) {
			return 18f * (rows + 1);
		}
	}

	public class Helper {
		public class LabelPair {
			public string propertyName;
			public string label       ;
			[Range(0f, 1f)] public float  percentage  ;
			public LabelPair(string propertyName, string label, float percentage) {
				this.propertyName = propertyName;
				this.label        = label       ;
				this.percentage   = percentage;
			}
		}
		public static void drawGUI(Rect position, SerializedProperty property, GUIContent label, LabelPair[] labelPairs) {
			label = EditorGUI.BeginProperty (position, label, property);
			label.text = "";
			Rect contentPos = EditorGUI.PrefixLabel  (position, label);
			float width = contentPos.width;
			EditorGUIUtility.labelWidth = 50f;
			contentPos.width = width;
			EditorGUI.indentLevel = 0;

			foreach (LabelPair labelPair in labelPairs) {
				contentPos.width = labelPair.percentage * width;
				EditorGUI.PropertyField (contentPos, property.FindPropertyRelative (labelPair.propertyName), new GUIContent (labelPair.label));
				contentPos.x += labelPair.percentage * width;
			}

			EditorGUI.EndProperty (); 
		}
	}
}

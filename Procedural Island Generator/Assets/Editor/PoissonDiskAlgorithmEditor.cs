﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PoissonDiskAlgorithm))]
public class PoissonDiskAlgorithmEditor : Editor {

	public override void OnInspectorGUI() {
		PoissonDiskAlgorithm algorithm = (PoissonDiskAlgorithm)target;

		DrawDefaultInspector ();
		if (GUILayout.Button ("Randomise")) {
			algorithm.RandomiseSeed ();
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Test))]
public class TestEditor : Editor {
	public override void OnInspectorGUI() {
		Test script = (Test)target;

		DrawDefaultInspector ();
		if (GUILayout.Button ("Test")) {
			script.RunTest ();
		}
	}
}

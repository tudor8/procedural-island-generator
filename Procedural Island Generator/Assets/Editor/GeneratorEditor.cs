﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Generator))]
public class GeneratorEditor : Editor {
	private bool previousExtrudeHeightsValue;
	public override void OnInspectorGUI() {
		Generator generator = (Generator)target;

		DrawDefaultInspector ();
		if (GUILayout.Button ("Generate New Seed")) {
			//generator.Randomize ();
			generator.Randomize ();
			generator.GenerateIsland ();
		}
		if (GUILayout.Button ("Generate")) {
			//generator.Randomize ();
			generator.GenerateIsland ();
		}
		if (GUILayout.Button ("Save As JSON")) {
			//generator.Randomize ();
			generator.Export();
		}
	}
}
